<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_karyawan".
 *
 * @property int $iId
 * @property string|null $vNip
 * @property string|null $vNama
 * @property string|null $vBirthPlace
 * @property string|null $dBirth
 * @property string|null $tAddress
 * @property string|null $vTelp
 * @property int|null $iRole
 * @property int|null $iActive
 * @property string|null $dEntry
 * @property int|null $iContract
 * @property string|null $eDeleted
 * @property string $tCreated
 * @property string $tUpdated
 */
class Karyawan extends \yii\db\ActiveRecord
{
    public $rolename;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_karyawan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dBirth', 'dEntry', 'tCreated', 'tUpdated','iWorkLocation','rolename'], 'safe'],
            [['tAddress', 'eDeleted'], 'string'],
            [['iRole', 'iActive', 'iContract'], 'integer'],
            [['vNip'], 'string', 'max' => 20],
            [['vNama', 'vBirthPlace'], 'string', 'max' => 100],
            [['vTelp'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'vNip' => 'NIP',
            'vNama' => 'Nama',
            'vBirthPlace' => 'Tempat Lahir',
            'dBirth' => 'Tanggal Lahir',
            'tAddress' => 'Alamat',
            'vTelp' => 'Telepon',
            'iRole' => 'Jabatan',
            'iActive' => 'Status',
            'dEntry' => 'Tanggal Gabung',
            'iContract' => 'Kontrak Kerja',
            'eDeleted' => 'E Deleted',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getRole(){
      return $this->hasOne(Role::className(), ['iId'=>'iRole']);
    }
}
