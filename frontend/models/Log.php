<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_role".
 *
 * @property int $iId
 * @property string|null $vRoleName
 * @property string|null $vKeterangan
 * @property string|null $eDeleted
 * @property string|null $eAktif
 * @property string $tCreated
 * @property string $tUpdated
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid'], 'string'],
            [['tCreated', 'tUpdated','eStatus','iTotal','dLast','id_user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'eStatus' => 'Status Login',
            'iTotal' => 'Total Login',
            'dLast' => 'Last Login',
            'tUpdated' => 'T Updated',
        ];
    }
}
