<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_kunjungan_dt".
 *
 * @property int $iId
 * @property int $iKunjunganHd
 * @property int|null $iOutletId
 * @property string|null $eJenis
 * @property string|null $vListProduk
 * @property string|null $eTipe
 * @property string|null $tKomentar
 * @property string $tCreated
 * @property string $tUpdated
 */
class KunjunganDt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_kunjungan_dt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iKunjunganHd'], 'required'],
            [['iKunjunganHd', 'iOutletId'], 'integer'],
            [['eJenis', 'eTipe', 'tKomentar'], 'string'],
            [['tCreated', 'tUpdated'], 'safe'],
            [['vListProduk'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'iKunjunganHd' => 'I Kunjungan Hd',
            'iOutletId' => 'I Outlet ID',
            'eJenis' => 'E Jenis',
            'vListProduk' => 'V List Produk',
            'eTipe' => 'E Tipe',
            'tKomentar' => 'T Komentar',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }
}
