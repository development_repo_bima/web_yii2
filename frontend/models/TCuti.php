<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_cuti".
 *
 * @property int $iId
 * @property int|null $iKaryawanId
 * @property string|null $dPeriodeCuti
 * @property int|null $iJumlah
 * @property string $tCreated
 * @property string $tUpdated
 */
class TCuti extends \yii\db\ActiveRecord
{
    public $flag = 'update';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_cuti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iJumlah'], 'integer'],
            [['eJenisCuti', 'tCreated', 'tUpdated','iJumlah','tKeterangan','dPosted','vLongitude','vLatitude','iKaryawanId','iReason', 'vText','flag', 'vLokasi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId'         => 'ID',
            'iKaryawanId' => 'Karyawan',
            'eJenisCuti'  => 'Jenis Izin',
            'iJumlah'     => 'Jumlah',
            'iReason'     => 'Alasan',
            'tKeterangan' => 'Keterangan',
            'dPosted'     => 'Tanggal Input',
            'vLongitude'  => 'Longitude',
            'vLatitude'   => 'latitude',
            'tCreated'    => 'T Created',
            'tUpdated'    => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iKaryawanId']);
    }
}
