<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_cuti".
 *
 * @property int $iId
 * @property int|null $iKaryawanId
 * @property string|null $dPeriodeCuti
 * @property int|null $iJumlah
 * @property string $tCreated
 * @property string $tUpdated
 */
class Cuti extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_cuti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iJumlah'], 'integer'],
            [['dPeriodeCuti', 'tCreated', 'tUpdated','ePublish','vSemester','iKaryawanId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'iKaryawanId' => 'Karyawan',
            'dPeriodeCuti' => 'Periode Cuti',
            'iJumlah'   => 'Jumlah',
            'ePublish'  => 'Terbitkan',
            'vSemester' => 'Semester',
            'tCreated'  => 'T Created',
            'tUpdated'  => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iKaryawanId']);
    }
}
