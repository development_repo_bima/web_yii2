<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_update".
 *
 * @property int $iId
 * @property string|null $vKeterangan
 * @property int|null $iConseq
 * @property string $tCreated
 * @property string $tUpdated
 */
class Update extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_update';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iConseq'], 'integer'],
            [['tCreated', 'tUpdated'], 'safe'],
            [['vKeterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'vKeterangan' => 'Keterangan',
            'iConseq' => 'Penalty Poin',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }
}
