<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_tree_structure".
 *
 * @property int $iId
 * @property int|null $iKaryawanId
 * @property int|null $iAtasanSuperId atasan tertinggi
 * @property int|null $iAtasanLangsungId atasan langsung
 * @property string|null $eJenis
 * @property string|null $eAktif
 * @property string $tCreated
 * @property string $tUpdated
 */
class TreeStructure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_tree_structure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['iKaryawanId', 'iAtasanSuperId', 'iAtasanLangsungId'], 'integer'],
            [['eJenis', 'eAktif'], 'string'],
            [['tCreated', 'tUpdated', 'iKaryawanId', 'iAtasanSuperId', 'iAtasanLangsungId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'iKaryawanId' => 'Karyawan',
            'iAtasanSuperId' => 'Atasan Superior',
            'iAtasanLangsungId' => 'Atasan Langsung',
            'eJenis' => 'Jenis Tim',
            'eAktif' => 'Aktif',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iKaryawanId']);
    }

    public function getAtasan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iAtasanLangsungId']);
    }

    public function getSuper(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iAtasanSuperId']);
    }
}
