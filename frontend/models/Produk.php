<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_produk".
 *
 * @property int $iId
 * @property string|null $vNama
 * @property string|null $vAlias
 * @property string|null $tExplain
 * @property int|null $iKemasan
 * @property string $tCreated
 * @property string $tUpdated
 */
class Produk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_produk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tExplain'], 'string'],
            [['iKemasan'], 'integer'],
            [['tCreated', 'tUpdated'], 'safe'],
            [['vNama', 'vAlias'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'vNama' => 'Nama',
            'vAlias' => 'Alias',
            'tExplain' => 'Penjelasan',
            'iKemasan' => 'Kemasan',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }
}
