<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_role".
 *
 * @property int $iId
 * @property string|null $vRoleName
 * @property string|null $vKeterangan
 * @property string|null $eDeleted
 * @property string|null $eAktif
 * @property string $tCreated
 * @property string $tUpdated
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eDeleted', 'eAktif'], 'string'],
            [['tCreated', 'tUpdated'], 'safe'],
            [['vRoleName', 'vKeterangan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'vRoleName' => 'Nama Jabatan',
            'vKeterangan' => 'Keterangan',
            'eDeleted' => 'Deleted',
            'eAktif' => 'Status Aktif',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }
}
