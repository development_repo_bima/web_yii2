<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_kunjungan".
 *
 * @property int $iId
 * @property int|null $iMarketingId
 * @property string|null $eType
 * @property string|null $dChecked
 * @property string|null $tKeterangan
 * @property string|null $vLongitude
 * @property string|null $vLatitude
 * @property string $tCreated
 * @property string $tUpdated
 */
class Kunjungan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_kunjungan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iMarketingId'], 'integer'],
            [['eType', 'tKeterangan'], 'string'],
            [['dChecked', 'tCreated', 'tUpdated','vText','vLokasi','iOutletId','iLengkap','dExpired'], 'safe'],
            [['vLongitude', 'vLatitude'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'iMarketingId' => 'I Marketing ID',
            'eType' => 'E Type',
            'dChecked' => 'D Checked',
            'tKeterangan' => 'T Keterangan',
            'vLongitude' => 'V Longitude',
            'vLatitude' => 'V Latitude',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }
}
