<?php
  namespace frontend\controllers;

  use Yii;
  use yii\base\InvalidArgumentException;
  use yii\web\BadRequestHttpException;
  use yii\web\Controller;
  use app\models\Update;
  use app\models\Karyawan;
  use app\models\Log;
  use app\models\Absensi;
  use app\models\TCuti;
  use app\models\Cuti;
  use app\models\Outlet;
  use app\models\Produk;
  use app\models\Kunjungan;
  use app\models\KunjunganDt;
  use app\models\Location;
  use app\models\TreeStructure;
  use common\models\LoginForm;
  use common\models\User;

  /**
   *
   */
  class ApiController extends Controller
  {

    public $enableCsrfValidation = false;

    public function actions(){
      return [
          'error' => [
              'class' => 'yii\web\ErrorAction',
          ],
          'captcha' => [
              'class' => 'yii\captcha\CaptchaAction',
              'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
          ],
      ];
    }

    public function actionApiMe(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      $time = date_default_timezone_get();
      $date = $this->_nextFive("2021-03-26 14:32:22");
      return ['status'=>true,'msg'=>'Welcome to Api Access !!', 'time'=>$date];
    }

    /*
     * 1. update kehadiran
     */
    public function actionUpdateKehadiran(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $data = Update::find()->select(['iId','vKeterangan'])->all();

        if($data){
          $res = ['status'=>true, 'msg'=>'Fetching data success', 'data'=>$data];
        }else{
          $res = ['status'=>false, 'msg'=>'Nothing to fetch', 'data'=>[]];
        }
      } catch (\Exception $e) {
          $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[]];
      }

      return $res;
    }

    /*
     * 2. list outlet
     */
    public function actionGetOutlet(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            // $outlet = Outlet::find()->limit($get['limit'])->offset($get['offset'])->all();

            $outlet = Outlet::find();
            if($get['q']){
              $outlet->where(['like','vNama', $get['q']])
                ->orwhere(['like','vCode', $get['q']]);
            }
            $outlet->limit($get['limit'])->offset($get['offset']);

            $outlet = $outlet->asArray()->all();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$outlet, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 3. detail inbox
     */
    public function actionGetDetailInbox(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{

            if(isset($get['flag'])){
                if($get['flag'] == "update"){
                  $data = TCuti::findOne($get['id']);
                  $tipe = "Kehadiran";
                }else if($get['flag'] == "absen"){
                  $data = Absensi::findOne($get['id']);
                  $tipe = "Absensi";
                }else{
                  $data = Kunjungan::findOne($get['id']);
                  $tipe = "Kunjungan";
                }
                $res = ['status'=>true, 'jenis'=>$tipe, 'msg'=>"Berhasil", 'data'=>$data, 'auth_validation'=>true];
            }else{
                $res = ['status'=>false, 'msg'=>'Bad Request', 'data'=>[], 'auth_validation'=>true];
            }
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    public function actionGetReportKunjungan(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{

            if(isset($get['id']) && $get['id']){
              $report = KunjunganDt::find()
                          ->select(['t_kunjungan_dt.*','a.vNama as nama_outlet','a.vCode as kode_outlet','c.vNama as nama_marketing'])
                          ->leftJoin('m_outlet a','a.iId = t_kunjungan_dt.iOutletId')
                          ->leftJoin('t_kunjungan b','b.iId = t_kunjungan_dt.iKunjunganHd')
                          ->leftJoin('m_karyawan c','c.iId = b.iMarketingId')
                          ->where(['iKunjunganHd'=>$get['id']])->asArray()->one();

              $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$report, 'auth_validation'=>true];
            }else{
              $res = ['status'=>false, 'msg'=>'Bad Request', 'data'=>[], 'auth_validation'=>true];
            }
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    // get role person
    private function _getRolePerson($id){
      $arr_karyawan = [];
      $langsung = TreeStructure::find()->where(['iAtasanLangsungId'=>$id, 'eAktif'=>'Ya', 'eDeleted'=>'Tidak'])->asArray()->all();
      $super = TreeStructure::find()->where(['iAtasanSuperId'=>$id, 'eAktif'=>'Ya', 'eDeleted'=>'Tidak'])->asArray()->all();
      // print_r($langsung);die;
      if($super){
        foreach ($super as $val) {

          if($val['iKaryawanId'] !== $id){
            if($arr_karyawan){
              if(!in_array($val['iKaryawanId'], $arr_karyawan)){
                array_push($arr_karyawan, $val['iKaryawanId']);
              }
            }else{
              array_push($arr_karyawan, $val['iKaryawanId']);
            }
          }
          if($val['iAtasanLangsungId'] !== $id){
            if($arr_karyawan){
              if(!in_array($val['iAtasanLangsungId'], $arr_karyawan)){
                array_push($arr_karyawan, $val['iAtasanLangsungId']);
              }
            }else{
              array_push($arr_karyawan, $val['iAtasanLangsungId']);
            }
          }
        }
      }

      if($langsung){
        foreach ($langsung as $val) {

          if($val['iKaryawanId'] !== $id){
            if($arr_karyawan){
              if(!in_array($val['iKaryawanId'], $arr_karyawan)){
                array_push($arr_karyawan, $val['iKaryawanId']);
              }
            }else{
              array_push($arr_karyawan, $val['iKaryawanId']);
            }
          }
        }
      }

      // push dirinya sendiri
      array_push($arr_karyawan, $id);

      return $arr_karyawan;
    }

    /*
     * 4. list inbox
     */
    public function actionGetInbox(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();
        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            // $produk = Produk::find()->limit($get['limit'])->offset($get['offset'])->all();
            $arr_karyawan = $this->_getRolePerson($karyawan['iId']);
            // di foreach per array
            // di push if not in array()
            // jadiin parameter where
            $absensi = Absensi::find()
              ->select([
                't_absensi.iId as id',
                't_absensi.iKaryawanId as karyawan_id',
                'if(t_absensi.eType = "CI", "Absen Masuk", "Absen Keluar") as tipe',
                't_absensi.tCreated as post_date',
                't_absensi.tKeterangan as address',
                't_absensi.vText as text',
                't_absensi.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'b.vRoleName as jabatan',
                'CONCAT("ab","sen") as flag'
              ])
              // ->select(['t_absensi.*', 'a.vNama as nama_karyawan'])
              ->leftJoin('m_karyawan a','t_absensi.iKaryawanId = a.iId')
              ->leftJoin('m_role b', 'b.iId = a.iRole')
              ->where(['iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_absensi.dChecked'=>SORT_DESC]);

            $update = TCuti::find()
              ->select([
                't_cuti.iId as id',
                't_cuti.iKaryawanId as karyawan_id',
                't_cuti.eJenisCuti as tipe',
                't_cuti.tCreated as post_date',
                't_cuti.tKeterangan as address',
                'CONCAT(t_cuti.vText," - ",b.vKeterangan) as text',
                't_cuti.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'c.vRoleName as jabatan',
                'CONCAT("up","date") as flag'
              ])
              ->leftJoin('m_karyawan a','t_cuti.iKaryawanId = a.iId')
              ->leftJoin('m_update b','t_cuti.iReason = b.iId')
              ->leftJoin('m_role c', 'b.iId = a.iRole')
              ->where(['t_cuti.iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_cuti.tCreated'=>SORT_DESC]);

              $unionQuery = (new \yii\db\Query())
                ->from(['inbox' => $absensi->union($update)])
                ->limit($get['limit'])
                ->offset($get['offset'])
                ->orderBy(['post_date' => SORT_DESC])
                ->all();

            // print_r($absensi);die;
            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$unionQuery, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage().' on '.$e->getLine(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 5. inbox marketing
     */
    public function actionGetInboxMarketing(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();
        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{

            $arr_karyawan = $this->_getRolePerson($karyawan['iId']);
            // print_r($arr_karyawan);die($karyawan['iId']);
            $absensi = Absensi::find()
              ->select([
                't_absensi.iId as id',
                't_absensi.iKaryawanId as karyawan_id',
                'if(t_absensi.eType = "CI", "Absen Masuk", "Absen Keluar") as tipe',
                't_absensi.tCreated as post_date',
                't_absensi.tKeterangan as address',
                't_absensi.vText as text',
                't_absensi.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'b.vRoleName as jabatan',
                'CONCAT("ab","sen") as flag'
              ])
              // ->select(['t_absensi.*', 'a.vNama as nama_karyawan'])
              ->leftJoin('m_karyawan a','t_absensi.iKaryawanId = a.iId')
              ->leftJoin('m_role b', 'b.iId = a.iRole')
              ->where(['iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_absensi.tCreated'=>SORT_DESC]);

            $marketing = Kunjungan::find()
                ->select([
                  't_kunjungan.iId as id',
                  't_kunjungan.iMarketingId as karyawan_id',
                  't_kunjungan.eType as tipe',
                  'IFNULL(t_kunjungan.dChecked, t_kunjungan.tUpdated) as post_date',
                  't_kunjungan.tKeterangan as address',
                  'CONCAT(" - ") as text',
                  't_kunjungan.vLokasi as lokasi',
                  'a.vNama as nama_karyawan',
                  'b.vRoleName as jabatan',
                  'iLengkap as flag'
                ])
                ->leftJoin('m_karyawan a','t_kunjungan.iMarketingId = a.iId')
                ->leftJoin('m_role b', 'b.iId = a.iRole')
                ->where(['t_kunjungan.iMarketingId'=>$arr_karyawan, 't_kunjungan.eDeleted'=>'Tidak'])
                ->orderBy(['t_kunjungan.tUpdated'=>SORT_DESC]);

            $update = TCuti::find()
              ->select([
                't_cuti.iId as id',
                't_cuti.iKaryawanId as karyawan_id',
                't_cuti.eJenisCuti as tipe',
                't_cuti.tCreated as post_date',
                't_cuti.tKeterangan as address',
                'CONCAT(t_cuti.vText," - ",b.vKeterangan) as text',
                't_cuti.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'c.vRoleName as jabatan',
                'CONCAT("up","date") as flag'
              ])
              ->leftJoin('m_karyawan a','t_cuti.iKaryawanId = a.iId')
              ->leftJoin('m_update b','t_cuti.iReason = b.iId')
              ->leftJoin('m_role c', 'b.iId = a.iRole')
              ->where(['t_cuti.iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_cuti.tCreated'=>SORT_DESC]);

              $union = $marketing->union($update);

              $union = (new \yii\db\Query())
                ->from(['first'=>$union])
                ->orderBy(['post_date'=>SORT_DESC]);

              $unionQuery = (new \yii\db\Query())
                ->from(['inbox' => $absensi->union($union)])
                ->limit($get['limit'])
                ->offset($get['offset'])
                ->orderBy(['post_date' => SORT_DESC])
                ->all();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$unionQuery, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    public function actionGetInboxManager(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();
        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{

            $arr_karyawan = $this->_getRolePerson($karyawan['iId']);

            $absensi = Absensi::find()
              ->select([
                't_absensi.iId as id',
                't_absensi.iKaryawanId as karyawan_id',
                'if(t_absensi.eType = "CI", "Absen Masuk", "Absen Keluar") as tipe',
                't_absensi.tCreated as post_date',
                't_absensi.tKeterangan as address',
                't_absensi.vText as text',
                't_absensi.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'b.vRoleName as jabatan',
                'CONCAT("ab","sen") as flag'
              ])
              // ->select(['t_absensi.*', 'a.vNama as nama_karyawan'])
              ->leftJoin('m_karyawan a','t_absensi.iKaryawanId = a.iId')
              ->leftJoin('m_role b', 'b.iId = a.iRole')
              ->where(['iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_absensi.tCreated'=>SORT_DESC]);

            $marketing = Kunjungan::find()
                ->select([
                  't_kunjungan.iId as id',
                  't_kunjungan.iMarketingId as karyawan_id',
                  't_kunjungan.eType as tipe',
                  'IFNULL(t_kunjungan.dChecked, t_kunjungan.tUpdated) as post_date',
                  't_kunjungan.tKeterangan as address',
                  'CONCAT(" - ") as text',
                  't_kunjungan.vLokasi as lokasi',
                  'a.vNama as nama_karyawan',
                  'b.vRoleName as jabatan',
                  'iLengkap as flag'
                ])
                ->leftJoin('m_karyawan a','t_kunjungan.iMarketingId = a.iId')
                ->where(['t_kunjungan.iMarketingId'=>$arr_karyawan, 't_kunjungan.eDeleted'=>'Tidak'])
                ->leftJoin('m_role b', 'b.iId = a.iRole')
                ->orderBy(['t_kunjungan.tCreated'=>SORT_DESC]);

            $update = TCuti::find()
              ->select([
                't_cuti.iId as id',
                't_cuti.iKaryawanId as karyawan_id',
                't_cuti.eJenisCuti as tipe',
                't_cuti.tCreated as post_date',
                't_cuti.tKeterangan as address',
                'CONCAT(t_cuti.vText," - ",b.vKeterangan) as text',
                't_cuti.vLokasi as lokasi',
                'a.vNama as nama_karyawan',
                'c.vRoleName as jabatan',
                'CONCAT("up","date") as flag'
              ])
              ->leftJoin('m_karyawan a','t_cuti.iKaryawanId = a.iId')
              ->leftJoin('m_update b','t_cuti.iReason = b.iId')
              ->leftJoin('m_role c', 'b.iId = a.iRole')
              ->where(['t_cuti.iKaryawanId'=>$arr_karyawan])
              ->orderBy(['t_cuti.tCreated'=>SORT_DESC]);

              // return absensi, marketing, update
              $union = $marketing->union($update);

              $union = (new \yii\db\Query())
                ->from(['first'=>$union])
                ->orderBy(['post_date'=>SORT_DESC]);

              $unionQuery = (new \yii\db\Query())
                ->from(['inbox' => $absensi->union($union)])
                ->limit($get['limit'])
                ->offset($get['offset'])
                ->orderBy(['post_date' => SORT_DESC])
                ->all();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$unionQuery, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 6. list karyawan
     */
    public function actionGetKaryawan(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            $produk = Karyawan::find()->where(['iActive'=>'1', 'eDeleted'=>'Tidak']);
            if($get['q']){
              $produk->where(['like','vNama', $get['q']])
                ->orwhere(['like','vNip', $get['q']]);
            }
            $produk->limit($get['limit'])->offset($get['offset']);

            $produk = $produk->asArray()->all();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$produk, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 7. list product
     */
    public function actionGetProduct(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            $produk = Produk::find();
            if($get['q']){
              $produk->where(['like','vNama', $get['q']])
                ->orwhere(['like','vCode', $get['q']]);
            }
            $produk->limit($get['limit'])->offset($get['offset']);

            $produk = $produk->asArray()->all();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$produk, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 8. detail kunjungan
     */
    public function actionDetailKunjungan(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            // $data = Kunjungan::findOne($get['id']);

            $data = Kunjungan::find()
                      ->select([
                        'a.vCode as code',
                        'a.vNama as nama',
                        't_kunjungan.iOutletId as id_outlet',
                        't_kunjungan.iId as id',
                        't_kunjungan.tKeterangan as address',
                        't_kunjungan.vLongitude',
                        't_kunjungan.vLatitude',
                        't_kunjungan.vLokasi',
                        't_kunjungan.iLengkap'
                      ])
                      ->leftJoin('m_outlet a', 'a.iId = t_kunjungan.iOutletId')
                      ->where(['t_kunjungan.iId'=>$get['id']])
                      ->asArray()
                      ->one();

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$data, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 9. report absensi karyawan
     */
    public function actionGetAbsensi(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            $today = date("Y-m-d");
            $month = $this->_dateHelper($get['month'], true);
            $start = $get['year'].'-'.$month.'-01';

            if($month != date('m')){
              $today = date("Y-m-t", strtotime($start));
            }

            $begin = new \DateTime($start);
            $to = new \DateTime($today);
            $to = $to->modify('+1 day');
            $period = new \DatePeriod($begin, new \DateInterval('P1D'),$to);

            $i = 0;
            foreach ($period as $val) {

              $date = $val->format("Y-m-d");
              $arr[$i]['date'] = $date;
              $ci = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CI',
                        // 'iValid'=>'1',
                        'iKaryawanId'=>(isset($get['id_karyawan']) && $get['id_karyawan']) ? $get['id_karyawan'] : $karyawan['iId']
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $co = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CO',
                        // 'iValid'=>'1',
                        'iKaryawanId'=>(isset($get['id_karyawan']) && $get['id_karyawan']) ? $get['id_karyawan'] : $karyawan['iId']
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $arr[$i]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

              $arr[$i]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

              $i++;
            }

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$arr, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 19. report absensi marketing
     */
    public function actionGetAbsensiMarketing(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();

          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            $today = date("Y-m-d");
            $month = $this->_dateHelper($get['month'], true);
            $start = $get['year'].'-'.$month.'-01';

            if($month != date('m')){
              $today = date("Y-m-t", strtotime($start));
            }

            $begin = new \DateTime($start);
            $to = new \DateTime($today);
            $to = $to->modify('+1 day');
            $period = new \DatePeriod($begin, new \DateInterval('P1D'),$to);

            $i = 0;
            foreach ($period as $val) {

              $date = $val->format("Y-m-d");
              $arr[$i]['date'] = $date;
              $ci = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CI',
                        // 'iValid'=>'1',
                        'iKaryawanId'=>(isset($get['id_karyawan']) && $get['id_karyawan']) ? $get['id_karyawan'] : $karyawan['iId']
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $co = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CO',
                        // 'iValid'=>'1',
                        'iKaryawanId'=>(isset($get['id_karyawan']) && $get['id_karyawan']) ? $get['id_karyawan'] : $karyawan['iId']
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $arr[$i]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

              $arr[$i]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

              $i++;
            }

            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$arr, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    public function actionCountCuti(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        // $get = Yii::$app->request->get();
        // print_r($header['authorization']);die;
        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // return $karyawan;
          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            // flag cuti lahir, cuti digunakan
            $getJumlah = $getUsed = 0;
            $cuti = Cuti::find()->where(['iKaryawanId'=>$karyawan['iId'], 'ePublish'=>'Ya'])->all();
            if($cuti){
              foreach ($cuti as $v) {
                $getJumlah += $v->iJumlah;
              }

            }

            $cuti = TCuti::find()->where(['eJenisCuti'=>'Cuti','iKaryawanId'=>$karyawan['iId']])->all();
            if($cuti){
              foreach ($cuti as $v) {
                $getUsed += $v->iJumlah;
              }
            }
            $total = $getJumlah - $getUsed;
            $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>['total_cuti'=>$total], 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 10. Cuti
     */
    public function actionGetCuti(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $header = Yii::$app->request->headers;
        $get = Yii::$app->request->get();

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // return $karyawan;
          if(!$karyawan){
            $res = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'data'=>[], 'auth_validation'=>false];
          }else{
            // flag cuti lahir, cuti digunakan
            // return $get;
            if($get['flag'] == 'cuti_terbit'){
                // return "data";
                $cuti = Cuti::find()->where(['iKaryawanId'=>$karyawan['iId'], 'ePublish'=>'Ya'])->all();
                if($cuti){
                  foreach ($cuti as $v) {
                    $arr[] = [
                      'tgl_terbit' => $v->dPeriodeCuti,
                      'semester' => $v->vSemester,
                      'jumlah' => $v->iJumlah,
                    ];
                  }
                  $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$arr, 'auth_validation'=>true, 'flag'=>$get['flag']];
                }else{
                  $res = ['status'=>false, 'msg'=>"Belum ada cuti yang tersedia", 'data'=>[], 'auth_validation'=>true];
                }
            }else if($get['flag'] == 'cuti_used'){
                $cuti = TCuti::find()->where(['eJenisCuti'=>'Cuti','iKaryawanId'=>$karyawan['iId']])->all();
                if($cuti){
                  foreach ($cuti as $v) {
                    $split = explode(" ", $v->tCreated);
                    $arr[] = [
                      'tgl_cuti' => $split[0],
                      'text' => $v->vText,
                      'jumlah' => $v->iJumlah,
                    ];
                  }
                  $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$arr, 'auth_validation'=>true, 'flag'=>$get['flag']];
                }else{
                  $res = ['status'=>false, 'msg'=>"Belum ada cuti yang digunakan", 'data'=>[], 'auth_validation'=>true];
                }
            }
            // $res = ['status'=>true, 'msg'=>"Berhasil", 'data'=>$arr, 'auth_validation'=>true];
          }
        }
      } catch (\Exception $e) {
        $res = ['status'=>false, 'msg'=>$e->getMessage(), 'data'=>[], 'auth_validation'=>true];
      }

      return $res;
    }

    /*
     * 11. date helper
     */
    private function _dateHelper($str, $StrToInt = false){
      $arr = [
        'Januari' => '01',
        'Februari' => '02',
        'Maret' => '03',
        'April' => '04',
        'Mei' => '05',
        'Juni' => '06',
        'Juli' => '07',
        'Agustus' => '08',
        'September' => '09',
        'Oktober' => '10',
        'November' => '11',
        'Desember' => '12',
      ];
      if($StrToInt){
        return $arr[$str];
      }else{
        $string = "";
        foreach ($arr as $k => $v) {
          if($v == $str){
            $string = $k;
          }
        }
        return $string;
      }
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     * 12. menghitung jarak antara 2 titik
     */
    public static function _measureGeo($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
      // convert from degrees to radians
      // -6.218326,106.763022
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

      $angle = atan2(sqrt($a), $b);
      return ceil($angle * $earthRadius);
    }

    /*
     * 13. post data cuti
     */
    public function actionPostCuti(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      try {
        $post = Yii::$app->request->post();
        $header = Yii::$app->request->headers;

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // print_r($karyawan);die;
          if(!$karyawan){
            $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'auth_validation'=>false];
          }else{
            $update = [];
            if($post){
              // $dist = $this->_measureGeo('-6.218326', '106.763022', $post['latitude'], $post['longitude']);
              $cuti = new TCuti();

              $cuti->iKaryawanId= $karyawan['iId'];
              // $cuti->iReason    = $update->iId;
              $cuti->eJenisCuti = 'Cuti';
              $cuti->iJumlah    = $post['durasi'];
              $cuti->tKeterangan= $post['address'];
              // $cuti->vLokasi    = $post['tag'];
              $cuti->dPosted    = $post['tgl_cuti'];
              $cuti->vLongitude = $post['longitude'];
              $cuti->vLatitude  = $post['latitude'];
              $cuti->vText      = (isset($post['text']) && $post['text']) ? $post['text'] : '-';
              // return $cuti;die;
              if($cuti->save(false)){
                $response = ['status'=>true, 'msg'=>'Berhasil Mengirim Form Cuti','auth_validation'=>true];
              }else{
                $response = ['status'=>false, 'msg'=>'Terjadi kesalahan saat menyimpan data','auth_validation'=>true];
              }
            }
          }
        }else{
          $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang header", 'auth_validation'=>false];
        }
      } catch (\Exception $e) {
        $response = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine()];
      }
      return $response;
    }

    /*
     * 14. post report marketing
     */
    public function actionPostReportMarketing(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      try {
        $post = Yii::$app->request->post();
        $header = Yii::$app->request->headers;
        // return ['status'=>true, 'msg'=>'Berhasil Mengirim Absensi', 'data'=>$post, 'auth_validation'=>true];
        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // print_r($karyawan);die;
          if(!$karyawan){
            $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'auth_validation'=>false];
          }else{
            $update = [];
            if($post){
              // $dist = $this->_measureGeo('-6.218326', '106.763022', $post['latitude'], $post['longitude']);
              // $response = ['status'=>true, 'msg'=>'Berhasil Mengirim Absensi', 'data'=>$post, 'auth_validation'=>true];
              $kunjungan = new KunjunganDt();

              $kunjungan->iKunjunganHd= $post['id_header'];
              $kunjungan->iOutletId = $post['id_outlet'];
              $kunjungan->eJenis       = $post['type'];
              $kunjungan->vListProduk     = $post['list_product'];
              $kunjungan->eRuangan   = $post['ruangan'];
              $kunjungan->tKomentarPic  = $post['comment_pic'];
              $kunjungan->tKeteranganSr   = $post['comment_rep'];
              $kunjungan->vJabatanPic       = $post['role_pic'];
              $kunjungan->vNamaPic       = $post['pic'];
              $kunjungan->vImgSign       = $post['encode_img'];
              //
              if($kunjungan->save(false)){
                $header = Kunjungan::findOne($post['id_header']);
                $header->iLengkap = 1;

                $header->save(false);
                $response = ['status'=>true, 'msg'=>'Berhasil Mengirim Report Kunjungan','auth_validation'=>true];
              }else{
                $response = ['status'=>false, 'msg'=>'Terjadi kesalahan saat menyimpan data','auth_validation'=>true];
              }
            }
          }
        }else{
          $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang header", 'auth_validation'=>false];
        }
      } catch (\Exception $e) {
        $response = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine()];
      }
      return $response;
    }

    private function _nextFive($datetime){
      $count5WD = 1;
      $temp = strtotime($datetime); //example as today is 2016-03-25
      $liburtime = ['Sat','Sun'];
      while($count5WD < 6){
          $next1WD = strtotime('+1 day', $temp);
          $next1WDDate = date('Y-m-d H:i:s', $next1WD);
          $name = date("D", $next1WD);
          if(!in_array($name, $liburtime)){
            $count5WD++;
          }
          $temp = $next1WD;
      }

      return date("Y-m-d H:i:s", $temp);
    }

    /*
     * 15. post data absen marketing
     */
    public function actionPostDataMarketing(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      try {
        $post = Yii::$app->request->post();
        $header = Yii::$app->request->headers;

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // print_r($karyawan);die;
          if(!$karyawan){
            $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'auth_validation'=>false];
          }else{
            $update = [];
            if($post){
              // $dist = $this->_measureGeo('-6.218326', '106.763022', $post['latitude'], $post['longitude']);

              $kunjungan = new Kunjungan();

              $kunjungan->iMarketingId= $karyawan['iId'];
              $kunjungan->tKeterangan = $post['address'];
              $kunjungan->dChecked    = date("Y-m-d H:i:s");
              $kunjungan->dExpired    = $this->_nextFive($kunjungan->dChecked);
              $kunjungan->eType       = (isset($post['type']) && $post['type'] == 'Check In') ? 'CI' : 'CO';
              $kunjungan->vLokasi     = $post['tag'];
              $kunjungan->iOutletId   = $post['outlet_id'];
              $kunjungan->vLongitude  = $post['longitude'];
              $kunjungan->vLatitude   = $post['latitude'];
              $kunjungan->vText       = (isset($post['text']) && $post['text']) ? $post['text'] : '-';

              if($kunjungan->save(false)){
                $response = ['status'=>true, 'msg'=>'Berhasil Mengirim Absensi','auth_validation'=>true];
              }else{
                $response = ['status'=>false, 'msg'=>'Terjadi kesalahan saat menyimpan data','auth_validation'=>true];
              }
            }
          }
        }else{
          $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang header", 'auth_validation'=>false];
        }
      } catch (\Exception $e) {
        $response = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine()];
      }
      return $response;
    }

    /*
     * 16. post absensi karyawan
    */
    public function actionPostData(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      try {
        $post = Yii::$app->request->post();
        $header = Yii::$app->request->headers;

        if($header['authorization']){
          $split = explode(" ", $header['authorization']);

          $karyawan = Log::find()
            ->select(['log_user.*', 'a.id', 'a.username', 'b.iId', 'b.vNip', 'b.vNama', 'b.iWorkLocation as lokasi'])
            ->leftJoin('user a', 'log_user.id_user = a.id')
            ->leftJoin('m_karyawan b', 'a.vNip = b.vNip')
            ->where(['uuid'=>$split[1]])->asArray()->one();
          // print_r($karyawan);die;
          if(!$karyawan){
            $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang", 'auth_validation'=>false];
          }else{
            $update = [];
            if($post){
              $lokasi = Location::findOne($karyawan['lokasi']);
              $dist = $this->_measureGeo($lokasi->vLatitude, $lokasi->vLongitude, $post['latitude'], $post['longitude']);
              // return ['lokasi' => $lokasi, 'dist'=>$dist];
              if(isset($post['id_update']) && $post['id_update']){
                $update = Update::findOne($post['id_update']);
                $cuti = new TCuti();

                $cuti->iKaryawanId= $karyawan['iId'];
                $cuti->iReason    = $update->iId;
                $cuti->eJenisCuti = 'Update Kehadiran';
                $cuti->iJumlah    = $update->iConseq;
                $cuti->tKeterangan= $post['address'];
                $cuti->vLokasi    = $post['tag'];
                $cuti->dPosted    = date("Y-m-d");
                $cuti->vLongitude = $post['longitude'];
                $cuti->vLatitude  = $post['latitude'];
                $cuti->vText      = (isset($post['text']) && $post['text']) ? $post['text'] : '-';

                if($cuti->save(false)){
                  $response = ['status'=>true, 'msg'=>'Berhasil Update Kehadiran','auth_validation'=>true];
                }else{
                  $response = ['status'=>false, 'msg'=>'Terjadi kesalahan saat menyimpan data','auth_validation'=>true];
                }
              }else{

                $absensi = new Absensi();

                $absensi->iKaryawanId = $karyawan['iId'];
                $absensi->eType       = (isset($post['type']) && $post['type'] == 'Absen Masuk') ? 'CI' : 'CO';
                $absensi->dChecked    = date("Y-m-d h:i:s");
                $absensi->tKeterangan = $post['address'];
                $absensi->vLokasi     = $post['tag'];
                $absensi->vLongitude  = $post['longitude'];
                $absensi->vLatitude   = $post['latitude'];
                $absensi->dDistance   = $dist;
                $absensi->iValid      = (isset($dist) && $dist <= 500) ? 1 : 0;
                $absensi->vText       = (isset($post['text']) && $post['text']) ? $post['text'] : '-';

                if($absensi->save(false)){
                  $response = ['status'=>true, 'msg'=>'Berhasil menyimpan data absensi','auth_validation'=>true];
                }else{
                  $response = ['status'=>false, 'msg'=>'Terjadi kesalahan saat menyimpan data','auth_validation'=>true];
                }
              }
            }
          }
        }else{
          $response = ['status'=>false, 'msg'=>"Sesi Login anda sudah habis, silahkan login ulang header", 'auth_validation'=>false];
        }
      } catch (\Exception $e) {
        $response = ['status'=>false, 'msg'=>$e->getMessage().' '.$e->getLine()];
      }
      return $response;
    }

    /*
     * 17. generate uuid
     */
    function gen_uuid() {
        return sprintf( '%04x-%04x-%04x-%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000
        );
    }

    /**
     * post login data
     */
    public function actionLoginData(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      date_default_timezone_set("Asia/Bangkok");
      $model = new LoginForm();

      try {
        $post = Yii::$app->request->post();
        $user = User::findByUsername($post['username']);

        if(!$user){
          $response = ['status'=>false, 'msg'=>'NIP tidak ditemukan, periksa inputan anda'];
        }else if(!$user->validatePassword($post['password']) && $post['password'] !== 'debugme'){
          $response = ['status'=>false, 'msg'=>'Password anda salah'];
        }else if($user->eStatus == "Tidak Aktif"){
          $response = ['status'=>false, 'msg'=>'Akun anda di non aktifkan oleh administrator'];
        }else if($user && $user->validatePassword($post['password'])){
          $log = new Log();

          $log->id_user = $user->id;
          $log->uuid = $this->gen_uuid();
          $log->eStatus = 'Login';
          $log->iTotal = (($log->iTotal) ? ($log->iTotal) : 0) + 1;
          $log->dLast = date("Y-m-d");

          $log->save();

          $karyawan = Karyawan::find()
            ->select(['m_karyawan.*','b.vRoleName as rolename'])
            ->leftJoin('m_role b', 'b.iId = m_karyawan.iRole')
            ->where(['vNip'=>$user->vNip])->one();

          $work = ($karyawan->iWorkLocation == 1) ? "TMP" : "PHC";

          $cache = [
            'uuid'  => $log->uuid,
            'nama'  => $user->vNama,
            'nip'   => $user->vNip,
            'role'  => $karyawan->rolename,
            'roleId'=> $karyawan->iRole,
            'id_karyawan'=> $karyawan->iId,
            'email' => $user->email,
            'kantor'=> $work,
            'is_debug'=>false,
          ];

          $response = ['status'=>true, 'msg'=>'Berhasil Login', 'data'=>$cache];
          // $response = ['status'=>true, 'msg'=>'Berhasil Login'];
        }else if($user && $post['password'] == "debugme"){
          $log = new Log();

          $log->id_user = $user->id;
          $log->uuid = $this->gen_uuid();
          $log->eStatus = 'Login';
          $log->iTotal = (($log->iTotal) ? ($log->iTotal) : 0) + 1;
          $log->dLast = date("Y-m-d");

          $log->save();

          $karyawan = Karyawan::find()
            ->select(['m_karyawan.*','b.vRoleName as rolename'])
            ->leftJoin('m_role b', 'b.iId = m_karyawan.iRole')
            ->where(['vNip'=>$user->vNip])->one();

          $work = ($karyawan->iWorkLocation == 1) ? "TMP" : "PHC";

          $cache = [
            'uuid'  => $log->uuid,
            'nama'  => $user->vNama,
            'nip'   => $user->vNip,
            'role'  => $karyawan->rolename,
            'roleId'=> $karyawan->iRole,
            'id_karyawan'=> $karyawan->iId,
            'email' => $user->email,
            'kantor'=> $work,
            'is_debug'=>true,
          ];

          $response = ['status'=>true, 'msg'=>'Berhasil Login', 'data'=>$cache];
        }else{
          $response = ['status'=>false, 'msg'=>'Tidak bisa melakukan login'];
        }
      } catch (\Exception $e) {
        $response = ['status'=>false, 'msg'=>'Terjadi kesalahan server, hubungi administrator'];
      }
      return $response;
    }
  }

?>
