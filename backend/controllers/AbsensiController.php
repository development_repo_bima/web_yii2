<?php

namespace backend\controllers;

use Yii;
use app\models\Absensi;
use app\models\Karyawan;
use app\models\TCuti;
use app\models\AbsensiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AbsensiController implements the CRUD actions for Absensi model.
 */
class AbsensiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Absensi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AbsensiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Absensi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Absensi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Absensi();

        if ($model->load(Yii::$app->request->post())) {
            if(!$model->datefrom){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal awal','status'=>false]);
              return $this->redirect(['create']);
            }else if(!$model->dateTo){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal akhir','status'=>false]);
              return $this->redirect(['create']);
            }else if($model->dateTo < $model->datefrom){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal akhir tidak boleh lebih awal dari tanggal awal','status'=>false]);
              return $this->redirect(['create']);
            }
            $begin = new \DateTime($model->datefrom);
            $to = new \DateTime($model->dateTo);
            $to = $to->modify('+1 day');
            $period = new \DatePeriod($begin, new \DateInterval('P1D'),$to);


            if($model->allKaryawan){
              $all = Karyawan::find()->select(['iId', 'vNama', 'iRole'])->asArray()->all();


              for ($i=0; $i < count($all); $i++) {
                $arr[$i]['karyawan'] = $all[$i]['vNama'];
                // $det = $arr[$i]['detail'];
                $x = 0;
                foreach ($period as $val) {

                  $date = $val->format("Y-m-d");

                  $day = date('D', strtotime($date));;
                  if($day == "Sat" || $day == "Sun"){
                    $text = "Hari Libur";
                  }else{
                    $text = '';
                  }

                  $det[$x]['date'] = $date;
                  $ci = Absensi::find()
                          ->where([
                            'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                            'eType'=>'CI',
                            // 'iValid'=>'1',
                            'iKaryawanId'=>$all[$i]['iId']
                          ])
                          ->orderBy(['iId'=>SORT_DESC])
                          ->one();

                  $co = Absensi::find()
                          ->where([
                            'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                            'eType'=>'CO',
                            // 'iValid'=>'1',
                            'iKaryawanId'=>$all[$i]['iId']
                          ])
                          ->orderBy(['iId'=>SORT_DESC])
                          ->one();

                  $izin = TCuti::find()
                            ->select(['t_cuti.*','a.vKeterangan as keterangan_update'])
                            ->leftJoin('m_update a','t_cuti.iReason = a.iId')
                            ->where([
                              'dPosted'=>$date,
                              'iKaryawanId'=>$all[$i]['iId']
                            ])
                            ->orderBy(['iId'=>SORT_DESC])
                            ->asArray()
                            ->one();

                  $string_in = 'Tidak ada Absen Masuk';
                  $string_out = 'Tidak ada Absen Pulang';
                  $lokasi_in = $lokasi_out = '';

                  if($ci && $ci->tCreated && !$text){
                    $late = $this->_timeDiff("Masuk",substr($ci->tCreated, 11, 8));
                    if($izin){
                      $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
                    }
                    $string_in = $late;
                    $lokasi_in = $ci->vLokasi.', '.$ci->tKeterangan;
                  }else if($text){
                    $string_in = $text;
                  }else if(!$ci && $izin){
                    $string_in = $izin['vText'].' - '.$izin['keterangan_update'];
                    $lokasi_in = $izin['vLokasi'].' - '.$izin['tKeterangan'];
                  }

                  if($co && $co->tCreated && !$text){
                    $late = $this->_timeDiff("Keluar",substr($co->tCreated, 11, 8));
                    // if($izin){
                    //   $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
                    // }
                    $string_out = $late;
                    $lokasi_out = $co->vLokasi.', '.$co->tKeterangan;
                  }else if($text){
                    $string_out = $text;
                  }else if(!$ci && $izin){
                    $string_out = $izin['vText'].' - '.$izin['keterangan_update'];
                    $lokasi_out = $izin['vLokasi'].' - '.$izin['tKeterangan'];
                  }

                  $det[$x]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

                  $det[$x]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

                  $det[$x]['selisih_in'] = $string_in;
                  $det[$x]['selisih_out'] = $string_out;
                  $det[$x]['lokasi_in'] = $lokasi_in;
                  $det[$x]['lokasi_out'] = $lokasi_out;

                  $x++;
                }
                $arr[$i]['detail'] = $det;
              }
              $tbl = '';
              $file = 'Report_Absensi_Semua_Karyawan_Periode_'.$model->datefrom.' s/d '.$model->dateTo.'.xls';
              for ($y=0; $y < count($arr); $y++) {


                $tbl .= '<table border="1" width="100%">';
                $tbl .= '<thead>';
                $tbl .= '<tr>';
                $tbl .= '<td colspan="7">Report Absensi '.$arr[$y]['karyawan'].' Periode '.$model->datefrom.' s/d '.$model->dateTo;
                $tbl .= '</td>';
                $tbl .= '</tr>';
                $tbl .= '<tr>';
                $tbl .= '<th>Tanggal</th>';
                $tbl .= '<th>Absen Masuk</th>';
                $tbl .= '<th>Absen Keluar</th>';
                $tbl .= '<th>Selisih Jam Masuk</th>';
                $tbl .= '<th>Selisih Jam Keluar</th>';
                $tbl .= '<th>Lokasi Absen Masuk</th>';
                $tbl .= '<th>Lokasi Absen Pulang</th>';
                $tbl .= '</tr></thead>';
                $tbl .= '<tbody>';
                foreach ($arr[$y]['detail'] as $k=>$v) {

                    $tbl .= '<tr>';
                    $tbl .= '<td>'.$v['date'].'</td>';
                    $tbl .= '<td>'.$v['ci'].'</td>';
                    $tbl .= '<td>'.$v['co'].'</td>';
                    $tbl .= '<td>'.$v['selisih_in'].'</td>';
                    $tbl .= '<td>'.$v['selisih_out'].'</td>';
                    $tbl .= '<td>'.$v['lokasi_in'].'</td>';
                    $tbl .= '<td>'.$v['lokasi_out'].'</td>';
                    $tbl .= '</tr>';
                }
                $tbl .= '</tbody>';
                $tbl .= '</table><br><br>';

              }
            }else{
              $i = 0;
              foreach ($period as $val) {

                $date = $val->format("Y-m-d");

                $day = date('D', strtotime($date));;
                if($day == "Sat" || $day == "Sun"){
                  $text = "Hari Libur";
                }else{
                  $text = '';
                }

                $arr[$i]['date'] = $date;
                $ci = Absensi::find()
                        ->where([
                          'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                          'eType'=>'CI',
                          // 'iValid'=>'1',
                          'iKaryawanId'=>(isset($model->iKaryawanId) && $model->iKaryawanId) ? $model->iKaryawanId : $model->iKaryawanId
                        ])
                        ->orderBy(['iId'=>SORT_DESC])
                        ->one();

                $co = Absensi::find()
                        ->where([
                          'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                          'eType'=>'CO',
                          // 'iValid'=>'1',
                          'iKaryawanId'=>(isset($model->iKaryawanId) && $model->iKaryawanId) ? $model->iKaryawanId : $model->iKaryawanId
                        ])
                        ->orderBy(['iId'=>SORT_DESC])
                        ->one();

                $izin = TCuti::find()
                          ->select(['t_cuti.*','a.vKeterangan as keterangan_update'])
                          ->leftJoin('m_update a','t_cuti.iReason = a.iId')
                          ->where([
                            'dPosted'=>$date,
                            'iKaryawanId'=>(isset($model->iKaryawanId) && $model->iKaryawanId) ? $model->iKaryawanId : $model->iKaryawanId
                          ])
                          ->orderBy(['iId'=>SORT_DESC])
                          ->asArray()
                          ->one();

                $string_in = 'Tidak ada Absen Masuk';
                $string_out = 'Tidak ada Absen Pulang';
                $lokasi_in = $lokasi_out = '';

                if($ci && $ci->tCreated && !$text){
                  $late = $this->_timeDiff("Masuk",substr($ci->tCreated, 11, 8));
                  if($izin){
                    $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
                  }
                  $string_in = $late;
                  $lokasi_in = $ci->vLokasi.', '.$ci->tKeterangan;
                }else if($text){
                  $string_in = $text;
                }else if(!$ci && $izin){
                  $string_in = $izin['vText'].' - '.$izin['keterangan_update'];
                  $lokasi_in = $izin['vLokasi'].' - '.$izin['tKeterangan'];
                }

                if($co && $co->tCreated && !$text){
                  $late = $this->_timeDiff("Keluar",substr($co->tCreated, 11, 8));
                  // if($izin){
                  //   $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
                  // }
                  $string_out = $late;
                  $lokasi_out = $co->vLokasi.', '.$co->tKeterangan;
                }else if($text){
                  $string_out = $text;
                }else if(!$ci && $izin){
                  $string_out = $izin['vText'].' - '.$izin['keterangan_update'];
                  $lokasi_out = $izin['vLokasi'].' - '.$izin['tKeterangan'];
                }

                $arr[$i]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

                $arr[$i]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

                $arr[$i]['selisih_in'] = $string_in;
                $arr[$i]['selisih_out'] = $string_out;
                $arr[$i]['lokasi_in'] = $lokasi_in;
                $arr[$i]['lokasi_out'] = $lokasi_out;



                $i++;
              }
              // print_r($arr);die;
              $karyawan = Karyawan::find()->where(['iId'=>$model->iKaryawanId])->one();
              // $file="demo.xls";
              $file = 'Report_Absensi_'.$karyawan->vNama.'_Periode_'.$model->datefrom.' s/d '.$model->dateTo.'.xls';
              $tbl = '<table border="1" width="100%">';
              $tbl .= '<thead>';
              $tbl .= '<tr>';
              $tbl .= '<td colspan="7">Report Absensi '.$karyawan->vNama.' Periode '.$model->datefrom.' s/d '.$model->dateTo;
              $tbl .= '</td>';
              $tbl .= '</tr>';
              $tbl .= '<tr>';
              $tbl .= '<th>Tanggal</th>';
              $tbl .= '<th>Absen Masuk</th>';
              $tbl .= '<th>Absen Keluar</th>';
              $tbl .= '<th>Selisih Jam Masuk</th>';
              $tbl .= '<th>Selisih Jam Keluar</th>';
              $tbl .= '<th>Lokasi Absen Masuk</th>';
              $tbl .= '<th>Lokasi Absen Pulang</th>';
              $tbl .= '</tr></thead>';
              $tbl .= '<tbody>';
              foreach ($arr as $k=>$v) {

                  $tbl .= '<tr>';
                  $tbl .= '<td>'.$v['date'].'</td>';
                  $tbl .= '<td>'.$v['ci'].'</td>';
                  $tbl .= '<td>'.$v['co'].'</td>';
                  $tbl .= '<td>'.$v['selisih_in'].'</td>';
                  $tbl .= '<td>'.$v['selisih_out'].'</td>';
                  $tbl .= '<td>'.$v['lokasi_in'].'</td>';
                  $tbl .= '<td>'.$v['lokasi_out'].'</td>';
                  $tbl .= '</tr>';
              }
              $tbl .= '</tbody>';
              $tbl .= '</table>';
            }


            $test="<table  ><tr><td>Cell 1</td><td>Cell 2</td></tr></table>";
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=$file");
            return $tbl;
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    private function _timeDiff($type="Masuk", $jam_absen){
      if($type == "Masuk"){
        $str_time = '08:00:00';
      }else{
        $str_time = '17:00:00';
      }
      if($jam_absen > $str_time && $type == "Masuk" || $jam_absen < $str_time && $type == "Keluar"){
        $dateTimeObject1 = date_create($str_time);
        $dateTimeObject2 = date_create($jam_absen);

        $difference = date_diff($dateTimeObject1, $dateTimeObject2);
        // echo ("The difference in hours is:");
        // echo "\n";
        $minutes = $difference->days * 24 * 60;
        $minutes += $difference->h * 60;
        $minutes += $difference->i;
        // echo("The difference in minutes is:");
        // echo $minutes.' minutes';

        return $minutes;
      }else{
        return 0;
      }
    }

    public function actionGenerate(){

        $model = new Absensi();

        if ($model->load(Yii::$app->request->post())) {
            print_r($model);die;
            $begin = new \DateTime($model->datefrom);
            $to = new \DateTime($model->dateTo);
            $to = $to->modify('+1 day');
            $period = new \DatePeriod($begin, new \DateInterval('P1D'),$to);

            $i = 0;
            foreach ($period as $val) {

              $date = $val->format("Y-m-d");
              $arr[$i]['date'] = $date;
              $ci = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CI',
                        'iValid'=>'1',
                        'iKaryawanId'=>(isset($model->iKaryawanId) && $model->iKaryawanId) ? $model->iKaryawanId : $model->iKaryawanId
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $co = Absensi::find()
                      ->where([
                        'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                        'eType'=>'CO',
                        'iValid'=>'1',
                        'iKaryawanId'=>(isset($model->iKaryawanId) && $model->iKaryawanId) ? $model->iKaryawanId : $model->iKaryawanId
                      ])
                      ->orderBy(['iId'=>SORT_DESC])
                      ->one();

              $arr[$i]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

              $arr[$i]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

              $i++;
            }

            print_r($arr);die;
        }



        // $file = \Yii::createObject([
        //     'class' => 'codemix\excelexport\ExcelFile',
        //     'sheets' => [
        //         'Users' => [
        //             'class' => 'codemix\excelexport\ActiveExcelSheet',
        //             'query' => User::find(),
        //         ]
        //     ]
        // ]);
        // $file->send('user.xlsx');
    }

    /**
     * Updates an existing Absensi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Absensi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Absensi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Absensi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Absensi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
