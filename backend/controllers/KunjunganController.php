<?php

namespace backend\controllers;

use Yii;
use app\models\Karyawan;
use app\models\Kunjungan;
use app\models\KunjunganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KunjunganController implements the CRUD actions for Kunjungan model.
 */
class KunjunganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kunjungan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KunjunganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kunjungan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionHapus($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eDeleted = "Ya";
            // $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan"
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Menghapus data kunjungan','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Kunjungan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kunjungan();

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->datefrom){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal awal','status'=>false]);
              return $this->redirect(['create']);
            }else if(!$model->dateTo){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal akhir','status'=>false]);
              return $this->redirect(['create']);
            }else if($model->dateTo < $model->datefrom){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Mohon pilih tanggal akhir tidak boleh lebih awal dari tanggal awal','status'=>false]);
              return $this->redirect(['create']);
            }
            if($model->allKaryawan){
              $all = Karyawan::find()->select(['iId', 'vNama', 'iRole'])->where(['eDeleted'=>'Tidak','iActive'=>'1','iRole'=>['5','6','13','15','19','22']])->asArray()->all();
              if(!$all){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Tidak ada data marketing','status'=>false]);
                return $this->redirect(['create']);
              }
              $file = 'Report_Kunjungan_'.$karyawan->vNama.'_Periode_'.$model->datefrom.' s/d '.$model->dateTo.'.xls';
              $tbl = '';
              for ($i=0; $i < count($all); $i++) {
                $kunjungan = Kunjungan::find()
                    ->select(['t_kunjungan.*', 'b.*','a.vNama as nama_outlet','a.vCode as kode_outlet',"DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d') as tgl_kunjungan", 't_kunjungan.tUpdated as jam_checkin'])
                    ->leftJoin('m_outlet a','a.iId = t_kunjungan.iOutletId')
                    ->leftJoin('t_kunjungan_dt b','b.iKunjunganHd = t_kunjungan.iId')
                    ->where(['t_kunjungan.iMarketingId'=>$all[$i]['iId'], 't_kunjungan.eType'=>'CI', 't_kunjungan.iLengkap'=>'1'])
                    ->andWhere(['between',"DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')", $model->datefrom, $model->dateTo])
                    ->orderBy(["DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')"=>SORT_ASC])
                    ->asArray()->all();

                $tbl .= '<table border="1" width="100%">';
                $tbl .= '<thead>';
                $tbl .= '<tr align="center">';
                $tbl .= '<td colspan="12">Report Kunjungan '.$all[$i]['vNama'].' Periode '.$model->datefrom.' s/d '.$model->dateTo;
                $tbl .= '</td>';
                $tbl .= '</tr>';
                $tbl .= '<tr>';
                $tbl .= '<th>Tanggal</th>';
                $tbl .= '<th>Nama Outlet</th>';
                $tbl .= '<th>Lokasi</th>';
                $tbl .= '<th>Jenis Kunjungan</th>';
                $tbl .= '<th>Ruangan</th>';
                $tbl .= '<th>Nama PIC</th>';
                $tbl .= '<th>Jabatan PIC</th>';
                $tbl .= '<th>Komentar PIC</th>';
                $tbl .= '<th>Komentar SR</th>';
                $tbl .= '<th>Produk yang Dibahas</th>';
                $tbl .= '<th>Jam Check in</th>';
                $tbl .= '<th>Jam Check out</th>';
                $tbl .= '</tr></thead>';
                $tbl .= '<tbody>';
                foreach ($kunjungan as $k=>$v) {

                    $co = Kunjungan::find()
                            ->where(['iOutletId'=>$v['iOutletId'], 'eType'=>'CO', "DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')" =>$v['tgl_kunjungan'], 'iMarketingId'=>$all[$i]['iId']])
                            ->orderBy(['iId'=>SORT_DESC])->one();
                    $tbl .= '<tr>';
                    $tbl .= '<td>'.$v['tgl_kunjungan'].'</td>';
                    $tbl .= '<td>'.$v['nama_outlet'].' - '.(($v['kode_outlet']) ? $v['kode_outlet'] : '').'</td>';
                    $tbl .= '<td>'.$v['vLokasi'].', '.$v['tKeterangan'].'</td>';
                    $tbl .= '<td>'.$v['eJenis'].'</td>';
                    $tbl .= '<td>'.$v['eRuangan'].'</td>';
                    $tbl .= '<td>'.$v['vNamaPic'].'</td>';
                    $tbl .= '<td>'.$v['vJabatanPic'].'</td>';
                    $tbl .= '<td>'.$v['tKomentarPic'].'</td>';
                    $tbl .= '<td>'.$v['tKeteranganSr'].'</td>';
                    $tbl .= '<td>'.$v['vListProduk'].'</td>';
                    $tbl .= '<td>'.$v['jam_checkin'].'</td>';
                    $tbl .= '<td>'.$co->tUpdated.'</td>';
                    $tbl .= '</tr>';
                }
                $tbl .= '</tbody>';
                $tbl .= '</table><br><br>';
              }
            }else{
              $kunjungan = Kunjungan::find()
                              ->select(['t_kunjungan.*', 'b.*','a.vNama as nama_outlet','a.vCode as kode_outlet',"DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d') as tgl_kunjungan",'t_kunjungan.tUpdated as jam_checkin'])
                              ->leftJoin('m_outlet a','a.iId = t_kunjungan.iOutletId')
                              ->leftJoin('t_kunjungan_dt b','b.iKunjunganHd = t_kunjungan.iId')
                              ->where(['t_kunjungan.iMarketingId'=>$model->idKaryawan, 't_kunjungan.eType'=>'CI', 't_kunjungan.iLengkap'=>'1'])
                              ->andWhere(['between',"DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')", $model->datefrom, $model->dateTo])
                              ->orderBy(["DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')"=>SORT_ASC])
                              ->asArray()->all();

              // print_r($kunjungan);die;

              $karyawan = Karyawan::find()->where(['iId'=>$model->idKaryawan])->one();


              $file = 'Report_Kunjungan_'.$karyawan->vNama.'_Periode_'.$model->datefrom.' s/d '.$model->dateTo.'.xls';
              $tbl = '<table border="1" width="100%">';
              $tbl .= '<thead>';
              $tbl .= '<tr align="center">';
              $tbl .= '<td colspan="12">Report Kunjungan '.$karyawan->vNama.' Periode '.$model->datefrom.' s/d '.$model->dateTo;
              $tbl .= '</td>';
              $tbl .= '</tr>';
              $tbl .= '<tr>';
              $tbl .= '<th>Tanggal</th>';
              $tbl .= '<th>Nama Outlet</th>';
              $tbl .= '<th>Lokasi</th>';
              $tbl .= '<th>Jenis Kunjungan</th>';
              $tbl .= '<th>Ruangan</th>';
              $tbl .= '<th>Nama PIC</th>';
              $tbl .= '<th>Jabatan PIC</th>';
              $tbl .= '<th>Komentar PIC</th>';
              $tbl .= '<th>Komentar SR</th>';
              $tbl .= '<th>Produk yang Dibahas</th>';
              $tbl .= '<th>Jam Check in</th>';
              $tbl .= '<th>Jam Check out</th>';
              $tbl .= '</tr></thead>';
              $tbl .= '<tbody>';
              foreach ($kunjungan as $k=>$v) {

                  $co = Kunjungan::find()
                          ->where(['iOutletId'=>$v['iOutletId'], 'eType'=>'CO', "DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')" =>$v['tgl_kunjungan'], 'iMarketingId'=>$model->idKaryawan])->one();
                  $tbl .= '<tr>';
                  $tbl .= '<td>'.$v['tgl_kunjungan'].'</td>';
                  $tbl .= '<td>'.$v['nama_outlet'].' - '.(($v['kode_outlet']) ? $v['kode_outlet'] : '').'</td>';
                  $tbl .= '<td>'.$v['vLokasi'].', '.$v['tKeterangan'].'</td>';
                  $tbl .= '<td>'.$v['eJenis'].'</td>';
                  $tbl .= '<td>'.$v['eRuangan'].'</td>';
                  $tbl .= '<td>'.$v['vNamaPic'].'</td>';
                  $tbl .= '<td>'.$v['vJabatanPic'].'</td>';
                  $tbl .= '<td>'.$v['tKomentarPic'].'</td>';
                  $tbl .= '<td>'.$v['tKeteranganSr'].'</td>';
                  $tbl .= '<td>'.$v['vListProduk'].'</td>';
                  $tbl .= '<td>'.$v['jam_checkin'].'</td>';
                  $tbl .= '<td>'.$co->tUpdated.'</td>';
                  $tbl .= '</tr>';
              }
              $tbl .= '</tbody>';
              $tbl .= '</table>';
            }

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=$file");
            return $tbl;

        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kunjungan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kunjungan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kunjungan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kunjungan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kunjungan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
