<?php

namespace backend\controllers;

use Yii;
use app\models\Role;
use app\models\RoleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\component\Reuse;

/**
 * RoleController implements the CRUD actions for Role model.
 */
class RoleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        // 'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Role models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Role model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionActive($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eAktif = $get['param'];
            $msg = ($get['param'] == "Ya") ? "Mengaktifkan" : "Menonaktifkan";
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil '.$msg.' Jabatan','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
        
    }

    public function actionHapus($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eDeleted = "Ya";
            // $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan"
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Menghapus data karyawan','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Creates a new Role model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Role();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambah data','status'=>true]);
              return $this->redirect(['index']);
            }else{
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
              return $this->redirect(['create']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Role model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil merubah data','status'=>true]);
              return $this->redirect(['index']);
            }else{
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
              return $this->redirect(['create']);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Role model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Role model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Role the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
