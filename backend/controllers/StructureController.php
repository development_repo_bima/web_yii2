<?php

namespace backend\controllers;

use Yii;
use app\models\TreeStructure;
use app\models\TreeStructureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StructureController implements the CRUD actions for TreeStructure model.
 */
class StructureController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TreeStructure models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TreeStructureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHapus($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eDeleted = "Ya";
            // $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan"
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Menghapus data','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }

    }

    /**
     * Displays a single TreeStructure model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionActive($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eAktif = $get['param'];
            $msg = ($get['param'] == "Ya") ? "Mengaktifkan" : "Menonaktifkan";
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil '.$msg,'status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }

    }

    /**
     * Creates a new TreeStructure model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TreeStructure();

        if ($model->load(Yii::$app->request->post())) {
            try {
                for ($i = 0; $i < count($model->iKaryawanId); $i++) {
                    $k = $model->iKaryawanId;

                    if($k[$i] == $model->iAtasanSuperId){
                        Yii::$app->getSession()->setFlash('return',['msg'=>'Periksa Input Karyawan anda. karyawan tidak bisa menjadi atasan superior untuk dirinya sendiri','status'=>false]);
                        return $this->redirect(['create']);
                    }else if($k[$i] == $model->iAtasanLangsungId){
                        Yii::$app->getSession()->setFlash('return',['msg'=>'Periksa Input Karyawan anda. karyawan tidak bisa menjadi atasan langsung untuk dirinya sendiri','status'=>false]);
                        return $this->redirect(['create']);
                    }else{
                        $perItem = new TreeStructure();

                        $perItem->iKaryawanId = $k[$i];
                        $perItem->iAtasanSuperId = $model->iAtasanSuperId;
                        $perItem->iAtasanLangsungId = $model->iAtasanLangsungId;
                        $perItem->eJenis = $model->eJenis;

                        // print_r($perItem);die;
                        $perItem->save(false);
                    }


                }

                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambah data','status'=>true]);
                return $this->redirect(['index']);

            } catch (\Exception $e) {
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
              return $this->redirect(['create']);
            }

            // return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TreeStructure model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Merubah data','status'=>true]);
                return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TreeStructure model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TreeStructure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TreeStructure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TreeStructure::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
