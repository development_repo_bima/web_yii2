<?php

namespace backend\controllers;

use Yii;
use app\models\Karyawan;
use app\models\KaryawanSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;


/**
 * KaryawanController implements the CRUD actions for Karyawan model.
 */
class KaryawanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        // 'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Karyawan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KaryawanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $pages = new Pagination(['totalCount'=> Karyawan::find()->count()]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider'=> $dataProvider,
            'pages'       => $pages,
            'index_title' => 'All Data'
        ]);
    }

    public function actionIndexPhc()
    {
        $searchModel = new KaryawanSearch();
        $dataProvider = $searchModel->searchPhc(Yii::$app->request->queryParams);
        $pages = new Pagination(['totalCount'=> Karyawan::find()->count()]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider'=> $dataProvider,
            'pages'       => $pages,
            'index_title' => 'PHC'
        ]);
    }

    public function actionIndexTmp()
    {
        $searchModel = new KaryawanSearch();
        $dataProvider = $searchModel->searchTmp(Yii::$app->request->queryParams);
        $pages = new Pagination(['totalCount'=> Karyawan::find()->count()]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider'=> $dataProvider,
            'pages'       => $pages,
            'index_title' => 'TMP'
        ]);
    }

    /**
     * Displays a single Karyawan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Karyawan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Karyawan();

        if ($model->load(Yii::$app->request->post())) {
            try {
              if($model->save()){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambah data','status'=>true]);
                return $this->redirect(['index']);
              }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
                return $this->redirect(['create']);
              }
            } catch (\Exception $e) {
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server','status'=>false]);
              return $this->redirect(['create']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionActive($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->iActive = $get['param'];
            $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan";
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil '.$msg.' karyawan','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
        
    }

    public function actionHapus($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eDeleted = "Ya";
            // $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan"
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Menghapus data karyawan','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Updates an existing Karyawan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
              $validate = Karyawan::find()->where(['iId'=>$model->iId])->one();

              $validate->vNip         = $model->vNip;
              $validate->vNama        = $model->vNama;
              $validate->vBirthPlace  = $model->vBirthPlace;
              $validate->dBirth       = $model->dBirth;
              $validate->tAddress     = $model->tAddress;
              $validate->vTelp        = $model->vTelp;
              $validate->iRole        = $model->iRole;
              $validate->iActive      = $model->iActive;
              $validate->dEntry       = $model->dEntry;
              $validate->iContract    = $model->iContract;
              $validate->iWorkLocation= $model->iWorkLocation;
              // if($validate){
              //   Yii::$app->getSession()->setFlash('return',['msg'=>'User tersebut sudah memiliki akun login','status'=>false]);
              //   return $this->redirect(['update?id='.$id]);
              // }
              // $data = Yii::$app->security->generatePasswordHash("satu");
              // $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);

              if($validate->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil merubah data','status'=>true]);
                return $this->redirect(['index']);
             }else{
               Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
               return $this->redirect(['update?id='.$id]);

             }
            } catch (\Exception $e) {
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
              return $this->redirect(['update?id='.$id]);
            }
            return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Karyawan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Karyawan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Karyawan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Karyawan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
