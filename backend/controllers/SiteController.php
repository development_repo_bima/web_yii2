<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use app\models\User;
use app\models\TCuti;
use app\models\Absensi;
use app\models\Kunjungan;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'get-absensi','get-marketing'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Absensi();
        $session = Yii::$app->session;
        $user = $session->get('user');

        if(!$user){
          Yii::$app->user->logout();

          return $this->goHome();
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionGetMarketing(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $get = Yii::$app->request->get();
        $from = date("Y-m-01");
        $to = date("Y-m-d");
        $kunjungan = Kunjungan::find()
            ->select(['t_kunjungan.*', 'b.*','a.vNama as nama_outlet','a.vCode as kode_outlet',"t_kunjungan.tUpdated as tgl_kunjungan"])
            ->leftJoin('m_outlet a','a.iId = t_kunjungan.iOutletId')
            ->leftJoin('t_kunjungan_dt b','b.iKunjunganHd = t_kunjungan.iId')
            ->where(['t_kunjungan.iMarketingId'=>$get['id'], 't_kunjungan.eType'=>'CI', 't_kunjungan.iLengkap'=>'1'])
            ->andWhere(['between',"DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')", $from, $to])
            ->orderBy(["DATE_FORMAT(t_kunjungan.tUpdated, '%Y-%m-%d')"=>SORT_ASC])
            ->asArray()->all();

        $result = ['status'=>true, 'data'=>$kunjungan, 'msg'=>'Berhasil'];
      } catch (\Exception $e) {
        $result = ['status'=>false, 'data'=>[], 'msg'=>'Terjadi Kesalahan server'];
      }

      return $result;
    }

    public function actionGetAbsensi(){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      try {
        $get = Yii::$app->request->get();
        // print_r($get);die;
        $today = date("Y-m-d");
        $begin = new \DateTime(date("Y-m-01"));
        $to = new \DateTime(date("Y-m-d"));
        $to = $to->modify('+1 day');
        $period = new \DatePeriod($begin, new \DateInterval('P1D'),$to);

        $i = 0;
        foreach ($period as $val) {

          $date = $val->format("Y-m-d");
          $day = $val->format("D");
          if($day == "Sat" || $day == "Sun"){
            $text = "Hari Libur";
          }else{
            $text = '';
          }
          $arr[$i]['date'] = $date;
          $ci = Absensi::find()
                  ->where([
                    'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                    'eType'=>'CI',
                    // 'iValid'=>'1',
                    'iKaryawanId'=>$get['id']
                  ])
                  ->orderBy(['iId'=>SORT_DESC])
                  ->one();

          $co = Absensi::find()
                  ->where([
                    'DATE_FORMAT(tCreated, "%Y-%m-%d")'=>$date,
                    'eType'=>'CO',
                    // 'iValid'=>'1',
                    'iKaryawanId'=>$get['id']
                  ])
                  ->orderBy(['iId'=>SORT_DESC])
                  ->one();

          $izin = TCuti::find()
                    ->select(['t_cuti.*','a.vKeterangan as keterangan_update'])
                    ->leftJoin('m_update a','t_cuti.iReason = a.iId')
                    ->where([
                      'dPosted'=>$date,
                      'iKaryawanId'=>$get['id']
                    ])
                    ->orderBy(['iId'=>SORT_DESC])
                    ->asArray()
                    ->one();
          $string_in = 'Tidak ada Absen Masuk';
          $string_out = 'Tidak ada Absen Pulang';

          if($ci && $ci->tCreated && !$text){
            $late = $this->_timeDiff("Masuk",substr($ci->tCreated, 11, 8));
            if($izin){
              $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
            }
            $string_in = $late;
          }else if($text){
            $string_in = $text;
          }else if(!$ci && $izin){
            $string_in = $izin['vText'].' - '.$izin['keterangan_update'];
          }

          if($co && $co->tCreated && !$text){
            $late = $this->_timeDiff("Keluar",substr($co->tCreated, 11, 8));
            // if($izin){
            //   $late .= ' menit, '.$izin['vText'].' - '.$izin['keterangan_update'];
            // }
            $string_out = $late;
          }else if($text){
            $string_out = $text;
          }else if(!$ci && $izin){
            $string_out = $izin['vText'].' - '.$izin['keterangan_update'];
          }

          $arr[$i]['ci'] = ($ci && $ci->tCreated) ? substr($ci->tCreated, 11, 8) : '-';

          $arr[$i]['co'] = ($co && $co->tCreated) ? substr($co->tCreated, 11, 8) : '-';

          $arr[$i]['selisih_in'] = $string_in;
          $arr[$i]['selisih_out'] = $string_out;

          $i++;
        }

        $result = ['status'=>true, 'data'=>$arr, 'msg'=>'Berhasil'];
      } catch (\Exception $e) {
        $result = ['status'=>false, 'data'=>[], 'msg'=>'Terjadi Kesalahan server'];
      }

      return $result;
    }

    private function _timeDiff($type="Masuk", $jam_absen){
      if($type == "Masuk"){
        $str_time = '08:00:00';
      }else{
        $str_time = '17:00:00';
      }
      if($jam_absen > $str_time && $type == "Masuk" || $jam_absen < $str_time && $type == "Keluar"){
        $dateTimeObject1 = date_create($str_time);
        $dateTimeObject2 = date_create($jam_absen);

        $difference = date_diff($dateTimeObject1, $dateTimeObject2);
        // echo ("The difference in hours is:");
        // echo "\n";
        $minutes = $difference->days * 24 * 60;
        $minutes += $difference->h * 60;
        $minutes += $difference->i;
        // echo("The difference in minutes is:");
        // echo $minutes.' minutes';

        return $minutes;
      }else{
        return 0;
      }

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        // if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post();
            $post = $post['LoginForm'];
            $user = User::find()
                      ->select(['user.*','a.iWorkLocation as worklocation','b.vRoleName as rolename'])
                      ->leftJoin('m_karyawan a','a.vNip = user.vNip')
                      ->leftJoin('m_role b','b.iId = a.iRole')
                      ->where(['username'=>$post['username']])->one();

            $user->worklocation = ($user->worklocation == 1) ? 'TMP' : 'PHC';
            $session = Yii::$app->session;
            $session->set('user',[
              'nama'          => $user->vNama,
              'nip'           => $user->vNip,
              'role'          => $user->rolename,
              'worklocation'  => $user->worklocation,
              'email'         => $user->email,
            ]);
            return $this->goHome();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                Yii::$app->getSession()->setFlash('success', [
                      'type' => 'success',
                      'icon' => 'fa fa-check-circle-o',
                      'message' => 'Berhasil menambahkan data',
               ]);
                    return $this->refresh();

            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
