<?php

namespace backend\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use backend\component\Reuse;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        // 'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            try {
              $validate = User::find()->where(['vNip'=>$model->vNip])->one();

              if($validate){
                Yii::$app->getSession()->setFlash('return',['msg'=>'User tersebut sudah memiliki akun login','status'=>false]);
                return $this->redirect(['create']);
              }
              // $data = Yii::$app->security->generatePasswordHash("satu");
              $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
              $model->status = 10;
              // var_dump($model->save(false));die;
              if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambahkan user','status'=>true]);
                return $this->redirect(['index']);
             }else{
               // print_r($model->getErrors());die;
               Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
               return $this->redirect(['create']);
             }
            } catch (\Exception $e) {
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
              return $this->redirect(['update?id='.$id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionReset($id){
      $model = $this->findModel($id);

      if($model->load(Yii::$app->request->post())){
        try {
          // print_r($model);die;
          $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);

          if($model->save(false)){
            Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Reset Password','status'=>true]);
            return $this->redirect(['index']);
         }else{
           // print_r($model->getErrors());die;
           Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
           return $this->redirect(['create']);
         }
        } catch (\Exception $e) {
          $recycle->falsePackage('Terjadi kesalahan server, mohon hubungi administrator', 'update?id='.$id);
          // Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
          return $this->redirect(['update?id='.$id]);
        }

      }

      return $this->render('view', [
          'model' => $model,
      ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
          try {
            $validate = User::find()->where(['vNip'=>$model->vNip])->one();

            $validate->eStatus = $model->eStatus;
            $recycle = new Reuse();
            if($validate->save(false)){
              $recycle->truePackage('Berhasil merubah data', 'index');
              // Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil merubah data','status'=>true]);
              return $this->redirect(['index']);
           }else{
             $recycle->falsePackage('Terjadi kesalahan saat menyimpan data, mohon hubungi administrator', 'update?id='.$id);
             // Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
             return $this->redirect(['update?id='.$id]);

           }
          } catch (\Exception $e) {
            $recycle->falsePackage('Terjadi kesalahan server, mohon hubungi administrator', 'update?id='.$id);
            // Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['update?id='.$id]);
          }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
