<?php

namespace backend\controllers;

use Yii;
use app\models\Update;
use app\models\UpdateSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UpdateController implements the CRUD actions for Update model.
 */
class UpdateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        // 'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Update models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UpdateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Update model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Update model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Update();

        if ($model->load(Yii::$app->request->post())) {
            try {
              if($model->save()){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambah data','status'=>true]);
                return $this->redirect(['index']);
              }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
                return $this->redirect(['create']);
              }
            } catch (\Exception $e) {
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server','status'=>false]);
              return $this->redirect(['create']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Update model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
              $validate = Update::find()->where(['iId'=>$model->iId])->one();

              $validate->vKeterangan = $model->vKeterangan;
              $validate->iConseq = $model->iConseq;

              if($validate->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil merubah data','status'=>true]);
                return $this->redirect(['index']);
             }else{
               // print_r($model->getErrors());die;
               Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
               return $this->redirect(['update?id='.$id]);

             }
            } catch (\Exception $e) {
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
              return $this->redirect(['update?id='.$id]);
            }
            return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Update model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Update model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Update the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Update::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
