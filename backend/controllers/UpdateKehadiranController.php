<?php

namespace backend\controllers;

use Yii;
use app\models\TCuti;
use app\models\Karyawan;
use app\models\TCutiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UpdateKehadiranController implements the CRUD actions for TCuti model.
 */
class UpdateKehadiranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TCuti models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TCutiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TCuti model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TCuti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TCuti();

        if ($model->load(Yii::$app->request->post())) {

            $update = TCuti::find()
                        ->select(['t_cuti.*', 'a.vKeterangan as alasan', 'a.iConseq as poin'])
                        ->leftJoin('m_update a', 'a.iId = t_cuti.iReason')
                        ->where(['iKaryawanId'=>$model->idKaryawan])
                        ->andWhere(['between','t_cuti.dPosted',$model->datefrom, $model->dateTo])
                        ->orderBy(['t_cuti.dPosted'=>SORT_ASC])
                        ->asArray()->all();

            $karyawan = Karyawan::find()->where(['iId'=>$model->idKaryawan])->one();

            
            $file = 'Report_Update_Kehadiran_'.$karyawan->vNama.'_Periode_'.$model->datefrom.' s/d '.$model->dateTo.'.xls';
            $tbl = '<table border="1" width="100%">';
            $tbl .= '<thead>';
            $tbl .= '<tr>';
            $tbl .= '<td colspan="5">Report Update Kehadiran '.$karyawan->vNama.' Periode '.$model->datefrom.' s/d '.$model->dateTo;
            $tbl .= '</td>';
            $tbl .= '</tr>';
            $tbl .= '<tr>';
            $tbl .= '<th>Tanggal</th>';
            $tbl .= '<th>Jenis Update</th>';
            $tbl .= '<th>Alasan</th>';
            $tbl .= '<th>Lokasi</th>';
            $tbl .= '<th>Poin</th>';
            $tbl .= '</tr></thead>';
            $tbl .= '<tbody>';
            foreach ($update as $k=>$v) {
                $tbl .= '<tr>';
                $tbl .= '<td>'.$v['dPosted'].'</td>';
                $tbl .= '<td>'.$v['eJenisCuti'].' - '.(($v['alasan']) ? $v['alasan'] : '').'</td>';
                $tbl .= '<td>'.$v['vText'].'</td>';
                $tbl .= '<td>'.$v['tKeterangan'].'</td>';
                $tbl .= '<td>'.$v['poin'].'</td>';
                $tbl .= '</tr>';
            }
            $tbl .= '</tbody>';
            $tbl .= '</table>';

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=$file");
            return $tbl;
        }else{
           return $this->render('create', [
                'model' => $model,
            ]); 
       }
    }

    /**
     * Updates an existing TCuti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TCuti model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TCuti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TCuti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TCuti::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
