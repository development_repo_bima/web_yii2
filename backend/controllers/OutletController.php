<?php

namespace backend\controllers;

use Yii;
use app\models\Outlet;
use app\models\OutletSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OutletController implements the CRUD actions for Outlet model.
 */
class OutletController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        // 'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Outlet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutletSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexTmp()
    {
        $searchModel = new OutletSearch();
        $dataProvider = $searchModel->searchTmp(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexPhc()
    {
        $searchModel = new OutletSearch();
        $dataProvider = $searchModel->searchPhc(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexCombo()
    {
        $searchModel = new OutletSearch();
        $dataProvider = $searchModel->searchCombo(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Outlet model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionHapus($id){
        try {
            $model = $this->findModel($id);
            $get = Yii::$app->request->get();
            $model->eDeleted = "Ya";
            // $msg = ($get['param'] == "1") ? "Mengaktifkan" : "Menonaktifkan"
            if($model->save(false)){
                Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil Menghapus data outlet','status'=>true]);
                return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data, mohon hubungi administrator','status'=>false]);
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan server, mohon hubungi administrator','status'=>false]);
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Creates a new Outlet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Outlet();

        if ($model->load(Yii::$app->request->post())) {

            if($model->iCombo){
              unset($model->iFieldforceAddId);
            }

            if($model->save()){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil menambah data','status'=>true]);
              return $this->redirect(['index']);
            }else{
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
              return $this->redirect(['create']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Outlet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->iCombo){
              // unset($model->iFieldforceAddId);
              $model->iFieldforceAddId = 0;
            }
            if($model->save()){
              Yii::$app->getSession()->setFlash('return',['msg'=>'Berhasil merubah data','status'=>true]);
              return $this->redirect(['index']);
            }else{
              Yii::$app->getSession()->setFlash('return',['msg'=>'Terjadi kesalahan saat menyimpan data','status'=>false]);
              return $this->redirect(['create']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Outlet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Outlet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Outlet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Outlet::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
