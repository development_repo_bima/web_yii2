<?php
  namespace backend\component;

  use Yii;

  /**
   * reuse class
   */
  class Reuse{

    public function truePackage($msg = '', $nextUrl = ''){
      try {
        Yii::$app->getSession()->setFlash('return',['msg'=>$msg,'status'=>true]);
        // return $controller->redirect([$nextUrl]);
      } catch (\Exception $e) {
        die($e->getMessage());
      }
    }

    public function falsePackage($msg = '', $nextUrl = ''){
      try {
        Yii::$app->getSession()->setFlash('return',['msg'=>$msg,'status'=>false]);
        // return $controller->redirect([$nextUrl]);
      } catch (\Exception $e) {
        die($e->getMessage());
      }
    }
  }

?>
