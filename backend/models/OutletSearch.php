<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Outlet;

/**
 * OutletSearch represents the model behind the search form of `app\models\Outlet`.
 */
class OutletSearch extends Outlet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId', 'iTOP'], 'integer'],
            [['vCode', 'vNama', 'eGroup', 'cTipe', 'eArea', 'tAddress', 'vTelp', 'tCreated', 'tUpdated','eStatus','iCombo','iFieldforceAddId','eDeleted','iFieldforceId','iSpvId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Outlet::find();
        $query->joinWith(['marketing a','marketingtmp b','spv c']);
        // add conditions that should always apply here
        $query->where(['m_outlet.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iFieldforceId' => $this->iFieldforceId,
            // 'iSpvId' => $this->iSpvId,
            'iTOP' => $this->iTOP,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'm_outlet.vNama', $this->vNama])
            ->andFilterWhere(['like', 'eGroup', $this->eGroup])
            ->andFilterWhere(['like', 'cTipe', $this->cTipe])
            ->andFilterWhere(['like', 'eArea', $this->eArea])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'a.vNama', $this->iFieldforceId])
            ->andFilterWhere(['like', 'b.vNama', $this->iFieldforceAddId])
            ->andFilterWhere(['like', 'c.vNama', $this->iSpvId])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchTmp($params)
    {
        $query = Outlet::find();
        $query->joinWith(['marketing a','marketingtmp b','spv c']);
        // add conditions that should always apply here
        $query->where(['!=','iFieldforceAddId','0']);
        $query->andWhere(['m_outlet.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iFieldforceId' => $this->iFieldforceId,
            // 'iSpvId' => $this->iSpvId,
            'iTOP' => $this->iTOP,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'm_outlet.vNama', $this->vNama])
            ->andFilterWhere(['like', 'eGroup', $this->eGroup])
            ->andFilterWhere(['like', 'cTipe', $this->cTipe])
            ->andFilterWhere(['like', 'eArea', $this->eArea])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'a.vNama', $this->iFieldforceId])
            ->andFilterWhere(['like', 'b.vNama', $this->iFieldforceAddId])
            ->andFilterWhere(['like', 'c.vNama', $this->iSpvId])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchPhc($params)
    {
        $query = Outlet::find();
        $query->joinWith(['marketing a','marketingtmp b','spv c']);
        // add conditions that should always apply here
        $query->where(['!=','iFieldforceId','0']);
        $query->andWhere(['m_outlet.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iFieldforceId' => $this->iFieldforceId,
            // 'iSpvId' => $this->iSpvId,
            'iTOP' => $this->iTOP,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'm_outlet.vNama', $this->vNama])
            ->andFilterWhere(['like', 'eGroup', $this->eGroup])
            ->andFilterWhere(['like', 'cTipe', $this->cTipe])
            ->andFilterWhere(['like', 'eArea', $this->eArea])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'a.vNama', $this->iFieldforceId])
            ->andFilterWhere(['like', 'b.vNama', $this->iFieldforceAddId])
            ->andFilterWhere(['like', 'c.vNama', $this->iSpvId])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchCombo($params)
    {
        $query = Outlet::find();
        $query->joinWith(['marketing a','marketingtmp b','spv c']);
        // add conditions that should always apply here
        $query->where(['iCombo'=>"1"]);
        $query->andWhere(['m_outlet.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iFieldforceId' => $this->iFieldforceId,
            // 'iSpvId' => $this->iSpvId,
            'iTOP' => $this->iTOP,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'm_outlet.vNama', $this->vNama])
            ->andFilterWhere(['like', 'eGroup', $this->eGroup])
            ->andFilterWhere(['like', 'cTipe', $this->cTipe])
            ->andFilterWhere(['like', 'eArea', $this->eArea])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'a.vNama', $this->iFieldforceId])
            ->andFilterWhere(['like', 'b.vNama', $this->iFieldforceAddId])
            ->andFilterWhere(['like', 'c.vNama', $this->iSpvId])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
