<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cuti;

/**
 * CutiSearch represents the model behind the search form of `app\models\Cuti`.
 */
class CutiSearch extends Cuti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId', 'iJumlah'], 'integer'],
            [['dPeriodeCuti', 'tCreated', 'tUpdated','ePublish','vSemester','iKaryawanId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cuti::find();
        $query->joinWith(['karyawan']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iKaryawanId' => $this->iKaryawanId,
            'dPeriodeCuti' => $this->dPeriodeCuti,
            'iJumlah' => $this->iJumlah,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNip', $this->vSemester])
              ->andFilterWhere(['like', 'm_karyawan.vNama', $this->iKaryawanId]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
