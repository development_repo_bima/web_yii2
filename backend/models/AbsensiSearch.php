<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Absensi;

/**
 * AbsensiSearch represents the model behind the search form of `app\models\Absensi`.
 */
class AbsensiSearch extends Absensi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId'], 'integer'],
            [['eType', 'dChecked', 'tKeterangan', 'vLongitude', 'vLatitude', 'tCreated', 'tUpdated','iKaryawanId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Absensi::find();
        $query->joinWith(['karyawan']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iKaryawanId' => $this->iKaryawanId,
            'dChecked' => $this->dChecked,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'eType', $this->eType])
            ->andFilterWhere(['like', 'tKeterangan', $this->tKeterangan])
            ->andFilterWhere(['like', 'vLongitude', $this->vLongitude])
            ->andFilterWhere(['like', 'm_karyawan.vNama', $this->iKaryawanId])
            ->andFilterWhere(['like', 'vLatitude', $this->vLatitude]);

        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
