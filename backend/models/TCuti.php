<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_cuti".
 *
 * @property int $iId
 * @property string|null $eJenisCuti
 * @property int|null $iJumlah
 * @property string|null $vKeterangan
 * @property string|null $dPosted
 * @property string|null $vLongitude
 * @property string|null $vLatitude
 * @property string $tCreated
 * @property string $tUpdated
 */
class TCuti extends \yii\db\ActiveRecord
{
    public $datefrom;
    public $dateTo;
    public $idKaryawan;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_cuti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eJenisCuti'], 'string'],
            [['iJumlah'], 'integer'],
            [['dPosted', 'tCreated', 'tUpdated','datefrom','dateTo','idKaryawan','iKaryawanId'], 'safe'],
            [['tKeterangan', 'vLongitude', 'vLatitude'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'eJenisCuti' => 'Jenis Cuti',
            'iJumlah' => 'Jumlah',
            'tKeterangan' => 'Keterangan',
            'iKaryawanId' => 'Nama Karyawan',
            'dPosted' => 'Posted',
            'vLongitude' => 'Longitude',
            'vLatitude' => 'Latitude',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iKaryawanId']);
    }

}
