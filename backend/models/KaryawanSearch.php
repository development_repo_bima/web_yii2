<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Karyawan;

/**
 * KaryawanSearch represents the model behind the search form of `app\models\Karyawan`.
 */
class KaryawanSearch extends Karyawan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId', 'iContract'], 'integer'],
            [['vNip','iActive','vNama','iRole','vBirthPlace', 'dBirth', 'tAddress', 'vTelp', 'dEntry', 'eDeleted', 'tCreated', 'tUpdated','iWorkLocation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Karyawan::find();
        $query->joinWith(['role']);
        // add conditions that should always apply here
        $query->where(['m_karyawan.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($this->iWorkLocation == 'TMP'){
          $this->iWorkLocation = '1';
        }else if($this->iWorkLocation == 'PHC'){
          $this->iWorkLocation = '2';
        }

        if($this->iActive == 'Aktif'){
          $this->iActive = '1';
        }else if($this->iActive == 'Tidak Aktif'){
          $this->iActive = '2';
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'dBirth' => $this->dBirth,
            // 'iRole' => $this->iRole,
            'iWorkLocation' => $this->iWorkLocation,
            'iActive' => $this->iActive,
            // 'dEntry' => $this->dEntry,
            'iContract' => $this->iContract,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNip', $this->vNip])
            ->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'm_role.vRoleName', $this->iRole])
            ->andFilterWhere(['like', 'vBirthPlace', $this->vBirthPlace])
            ->andFilterWhere(['like', 'dBirth', $this->dBirth])
            ->andFilterWhere(['like', 'dEntry', $this->dEntry])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp])
            ->andFilterWhere(['like', 'm_karyawan.eDeleted', $this->eDeleted]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchPhc($params)
    {
        $query = Karyawan::find();
        $query->joinWith(['role']);
        // add conditions that should always apply here
        $query->where(['iWorkLocation'=>'2', 'm_karyawan.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($this->iWorkLocation == 'TMP'){
          $this->iWorkLocation = '1';
        }else if($this->iWorkLocation == 'PHC'){
          $this->iWorkLocation = '2';
        }

        if($this->iActive == 'Aktif'){
          $this->iActive = '1';
        }else if($this->iActive == 'Tidak Aktif'){
          $this->iActive = '2';
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'dBirth' => $this->dBirth,
            // 'iRole' => $this->iRole,
            'iWorkLocation' => $this->iWorkLocation,
            'iActive' => $this->iActive,
            // 'dEntry' => $this->dEntry,
            'iContract' => $this->iContract,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNip', $this->vNip])
            ->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'm_role.vRoleName', $this->iRole])
            ->andFilterWhere(['like', 'vBirthPlace', $this->vBirthPlace])
            ->andFilterWhere(['like', 'dBirth', $this->dBirth])
            ->andFilterWhere(['like', 'dEntry', $this->dEntry])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp])
            ->andFilterWhere(['like', 'm_karyawan.eDeleted', $this->eDeleted]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchTmp($params)
    {
        $query = Karyawan::find();
        $query->joinWith(['role']);
        // add conditions that should always apply here
        $query->where(['iWorkLocation'=>'1', 'm_karyawan.eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($this->iWorkLocation == 'TMP'){
          $this->iWorkLocation = '1';
        }else if($this->iWorkLocation == 'PHC'){
          $this->iWorkLocation = '2';
        }

        if($this->iActive == 'Aktif'){
          $this->iActive = '1';
        }else if($this->iActive == 'Tidak Aktif'){
          $this->iActive = '2';
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'dBirth' => $this->dBirth,
            // 'iRole' => $this->iRole,
            'iWorkLocation' => $this->iWorkLocation,
            'iActive' => $this->iActive,
            // 'dEntry' => $this->dEntry,
            'iContract' => $this->iContract,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNip', $this->vNip])
            ->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'm_role.vRoleName', $this->iRole])
            ->andFilterWhere(['like', 'vBirthPlace', $this->vBirthPlace])
            ->andFilterWhere(['like', 'dBirth', $this->dBirth])
            ->andFilterWhere(['like', 'dEntry', $this->dEntry])
            ->andFilterWhere(['like', 'tAddress', $this->tAddress])
            ->andFilterWhere(['like', 'vTelp', $this->vTelp])
            ->andFilterWhere(['like', 'm_karyawan.eDeleted', $this->eDeleted]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
