<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TCuti;

/**
 * TCutiSearch represents the model behind the search form of `app\models\TCuti`.
 */
class TCutiSearch extends TCuti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId', 'iJumlah'], 'integer'],
            [['eJenisCuti', 'tKeterangan', 'dPosted', 'vLongitude', 'vLatitude', 'tCreated', 'tUpdated','iKaryawanId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TCuti::find();
        $query->joinWith(['karyawan']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            'iJumlah' => $this->iJumlah,
            'dPosted' => $this->dPosted,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'eJenisCuti', $this->eJenisCuti])
            ->andFilterWhere(['like', 'tKeterangan', $this->tKeterangan])
            ->andFilterWhere(['like', 'vLongitude', $this->vLongitude])
            ->andFilterWhere(['like', 'm_karyawan.vNama', $this->iKaryawanId])
            ->andFilterWhere(['like', 'vLatitude', $this->vLatitude]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
