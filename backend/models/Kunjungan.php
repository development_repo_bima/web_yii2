<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_kunjungan".
 *
 * @property int $iId
 * @property int|null $iMarketingId
 * @property string|null $eType
 * @property string|null $dChecked
 * @property string|null $tKeterangan
 * @property string|null $vLongitude
 * @property string|null $vLatitude
 * @property string $tCreated
 * @property string $tUpdated
 */
class Kunjungan extends \yii\db\ActiveRecord
{
    public $datefrom;
    public $dateTo;
    public $idKaryawan;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_kunjungan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [[''], 'integer'],
            [['eType', 'tKeterangan'], 'string'],
            [['dChecked', 'tCreated', 'tUpdated','iMarketingId','datefrom','dateTo','idKaryawan','iOutletId','iLengkap','dExpired'], 'safe'],
            [['vLongitude', 'vLatitude'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'iMarketingId' => 'Nama Marketing',
            'eType' => 'Jenis Absensi',
            'dChecked' => 'D Checked',
            'tKeterangan' => 'Keterangan',
            'vLongitude' => 'V Longitude',
            'vLatitude' => 'V Latitude',
            'tCreated' => 'Waktu',
            'tUpdated' => 'T Updated',
        ];
    }


    public function getMarketing(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iMarketingId']);
    }
}
