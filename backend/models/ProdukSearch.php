<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Produk;

/**
 * ProdukSearch represents the model behind the search form of `app\models\Produk`.
 */
class ProdukSearch extends Produk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId'], 'integer'],
            [['tCreated', 'tUpdated','vNama', 'vAlias','vCode','dHna','dPpn','dTotal','vKemasan','eDistributor','vBrand','vPrincipal','eDeleted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Produk::find();

        // add conditions that should always apply here
        $query->where(['eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'vBrand', $this->vBrand])
            ->andFilterWhere(['like', 'vPrincipal', $this->vPrincipal])
            ->andFilterWhere(['like', 'vAlias', $this->vAlias])
            ->andFilterWhere(['like', 'tExplain', $this->tExplain]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchPhc($params)
    {
        $query = Produk::find();

        // add conditions that should always apply here
        $query->where(['eDistributor'=>'PHC','eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'vBrand', $this->vBrand])
            ->andFilterWhere(['like', 'vPrincipal', $this->vPrincipal])
            ->andFilterWhere(['like', 'vAlias', $this->vAlias])
            ->andFilterWhere(['like', 'tExplain', $this->tExplain]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }

    public function searchTmp($params)
    {
        $query = Produk::find();

        // add conditions that should always apply here
        $query->where(['eDistributor'=>'TMP','eDeleted'=>'Tidak']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'vNama', $this->vNama])
            ->andFilterWhere(['like', 'vCode', $this->vCode])
            ->andFilterWhere(['like', 'vBrand', $this->vBrand])
            ->andFilterWhere(['like', 'vPrincipal', $this->vPrincipal])
            ->andFilterWhere(['like', 'vAlias', $this->vAlias])
            ->andFilterWhere(['like', 'tExplain', $this->tExplain]);
        $query->orderBy(['iId'=>SORT_DESC]);
        return $dataProvider;
    }
}
