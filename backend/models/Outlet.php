<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_outlet".
 *
 * @property int $iId
 * @property string|null $vCode
 * @property string|null $vNama
 * @property string|null $eGroup
 * @property string|null $cTipe
 * @property string|null $eArea
 * @property int|null $iFieldforceId
 * @property int|null $iSpvId
 * @property int|null $iTOP
 * @property string|null $tAddress
 * @property string|null $vTelp
 * @property string $tCreated
 * @property string $tUpdated
 */
class Outlet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_outlet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eGroup', 'eArea', 'tAddress'], 'string'],
            [['iFieldforceId','iFieldforceAddId','iSpvId', 'iTOP'], 'integer'],
            [['tCreated', 'tUpdated', 'iCombo','eStatus','eDeleted'], 'safe'],
            [['vCode', 'vNama'], 'string', 'max' => 100],
            [['cTipe'], 'string', 'max' => 2],
            [['vTelp'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'I ID',
            'vCode' => 'Kode Outlet',
            'vNama' => 'Nama Outlet',
            'eGroup' => 'Group',
            'cTipe' => 'Tipe',
            'eArea' => 'Area',
            'iFieldforceId' => 'Medrep PHC',
            'iFieldforceAddId' => 'Medrep TMP',
            'iCombo'  => 'Flag Combo',
            'iSpvId' => 'SPV',
            'iTOP' => 'TOP',
            'tAddress' => 'Alamat',
            'vTelp' => 'Telepon',
            'tCreated' => 'T Created',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getMarketing(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iFieldforceId']);
    }

    public function getMarketingtmp(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iFieldforceAddId']);
    }

    public function getSpv(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iSpvId']);
    }
}
