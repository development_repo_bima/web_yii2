<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TreeStructure;

/**
 * TreeStructureSearch represents the model behind the search form of `app\models\TreeStructure`.
 */
class TreeStructureSearch extends TreeStructure
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iId'], 'integer'],
            [['eJenis', 'eAktif', 'tCreated', 'tUpdated', 'iKaryawanId', 'iAtasanSuperId', 'iAtasanLangsungId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TreeStructure::find();
        $query->joinWith(['karyawan kar', 'atasan at', 'super sup']);
        // $query->joinWith(['atasan']);
        // $query->joinWith(['super']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iId' => $this->iId,
            // 'iKaryawanId' => $this->iKaryawanId,
            // 'iAtasanSuperId' => $this->iAtasanSuperId,
            // 'iAtasanLangsungId' => $this->iAtasanLangsungId,
            'tCreated' => $this->tCreated,
            'tUpdated' => $this->tUpdated,
        ]);

        $query->andFilterWhere(['like', 'eJenis', $this->eJenis])
            ->andFilterWhere(['like', 'kar.vNama', $this->iKaryawanId])
            ->andFilterWhere(['like', 'at.vNama', $this->iAtasanLangsungId])
            ->andFilterWhere(['like', 'sup.vNama', $this->iAtasanSuperId])
            ->andFilterWhere(['like', 'eAktif', $this->eAktif]);
        $query->orderBy(['iId'=>SORT_DESC]);
        // print_r($query);die;
        return $dataProvider;
    }
}
