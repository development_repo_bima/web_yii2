<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_absensi".
 *
 * @property int $iId
 * @property int $iKaryawanId
 * @property string|null $eType
 * @property string|null $dChecked
 * @property string|null $tKeterangan
 * @property string|null $vLongitude
 * @property string|null $vLatitude
 * @property string $tCreated
 * @property string $tUpdated
 */
class Absensi extends \yii\db\ActiveRecord
{
    public $datefrom;
    public $dateTo;
    public $idKaryawan;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_absensi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iKaryawanId'], 'required'],
            [['iKaryawanId'], 'integer'],
            [['eType', 'tKeterangan'], 'string'],
            [['dChecked', 'tCreated', 'tUpdated','datefrom','dateTo','idKaryawan'], 'safe'],
            [['vLongitude', 'vLatitude'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iId' => 'ID',
            'iKaryawanId' => 'Karyawan',
            'eType' => 'Tipe Absensi',
            'dChecked' => 'Checked',
            'tKeterangan' => 'Keterangan',
            'vLongitude' => 'Longitude',
            'vLatitude' => 'Latitude',
            'tCreated' => 'Waktu',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['iId'=>'iKaryawanId']);
    }
}
