<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string|null $vNama
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $tUpdated
 */
class User extends \yii\db\ActiveRecord
{
    public $password, $repassword, $rolename, $worklocation;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'status', 'vNip','eStatus'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['tUpdated', 'password', 'repassword','rolename','worklocation'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['vNama', 'auth_key'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'vNama' => 'Nama User',
            'vNip' => 'NIP',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'eStatus' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tUpdated' => 'T Updated',
        ];
    }

    public function getKaryawan(){
      return $this->hasOne(Karyawan::className(), ['vNip'=>'vNip']);
    }
}
