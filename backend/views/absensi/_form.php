<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\select2\Widget;
use buibr\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Absensi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="absensi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field($model, 'iKaryawanId')->widget(Widget::className(), [
            'options' => [
                'multiple' => false,
                'placeholder' => 'Pilih NIP',
                'onchange'   => 'clickable(this);',
                'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                            ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                            ->orderBy('vNama')->all(), 'vNip', function($m){
                                return ['data-vnama'=>$m->vNama];
                            }),
            ],
            'settings' => [
                'width' => '100%',
            ],
            'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                        ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                        ->orderBy('vNama')->all(), 'iId', function($m){
                            return '('.$m->vNip.') - '.$m->vNama.' - '.$m->role->vRoleName;
                        }),
            'events' => []
          ]);
    ?>

    <?= $form->field($model, 'datefrom')->widget(
                DatePicker::className(), [
                    'addon' => false,
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'stepping' => 30,
                    ],
            ]);?>

    <?= $form->field($model, 'dateTo')->widget(
                DatePicker::className(), [
                    'addon' => false,
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'stepping' => 30,
                    ],
            ]);?>


    <div class="form-group">
        <?= Html::submitButton('Generate', ['class' => 'btn btn-success']) ?>
        <!-- <?= Html::a('Generate', ['generate'], ['class' => 'btn btn-success']) ?> -->
    </div>

    <?php ActiveForm::end(); ?>

</div>
