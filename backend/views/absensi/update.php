<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Absensi */

$this->title = 'Update Absensi: ' . $model->iId;
$this->params['breadcrumbs'][] = ['label' => 'Absensi Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iId, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="absensi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
