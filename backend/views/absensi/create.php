<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Absensi */

$this->title = 'Generate Report Absensi';
$this->params['breadcrumbs'][] = ['label' => 'Absensi Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absensi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
