<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use buibr\datepicker\DatePicker;
use vova07\select2\Widget;

$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$isUpdate = (isset($_GET['id']) && $_GET['id']) ? true : false;

?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<script>var is_update = "<?= $isUpdate ?>"</script>
<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-8">
            <?php
            if(!$isUpdate){
            echo $form->field($model, 'username')->widget(Widget::className(), [
                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Pilih NIP',
                    'onchange'   => 'clickable(this);',
                    'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                ->orderBy('vNama')->all(), 'vNip', function($m){
                                    return ['data-vnama'=>$m->vNama];
                                }),
                ],
                'settings' => [
                    'width' => '100%',
                ],
                'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                            ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                            ->orderBy('vNama')->all(), 'vNip', function($m){
                                return $m->vNip;
                            }),
                'events' => []
              ]);
            }else{
              echo $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly'=>'true']);
            }
            ?>
          </div>
          <div class="col-md-4">
            <?php
            echo $form->field($model, 'eStatus')->widget(Widget::className(), [
                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Choose item'
                ],
                'settings' => [
                    'width' => '100%',
                ],
                'items' => [
                    'Aktif'=>'Aktif',
                    'Tidak Aktif'=>'Tidak Aktif',
                    'Hold'  => 'Hold'
                ],
                'events' => [
                    // 'select2-open' => 'function (e) { log("select2:open", e); }',
                ]
              ]);
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'vNip')->textInput(['maxlength' => true, 'readonly'=>'true']) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'vNama')->textInput(['maxlength' => true, 'readonly'=>'true']) ?>
          </div>
        </div>
      </div>
      <?php if(!$isUpdate){ ?>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
              <?= $form->field($model, 'repassword')->passwordInput(['maxlength' => true, 'onblur'=>'verifikasi(this);']) ?>
            </div>
          </div>

        </div>
      <?php } ?>
    </div>


    <!-- <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'tUpdated')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'submit-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);

  $('#submit-button').click(function(){
    return validate();
  });
});

function validate(){

  if(!$('#user-password').val() && is_update == ""){
    customAlert.gagal("Harap mengisi password anda");
    return false;
  }else if(!$('#user-repassword').val() && is_update == ""){
    customAlert.gagal("Harap mengisi re password anda");
    return false;
  }else if(!$('#user-vnama').val() || !$('#user-vnip').val() && is_update == ""){
    customAlert.gagal("Harap memilih username anda");
    return false;
  }
  // return false;
};
function clickable(obj){
  let value = $(obj).val();
  var val = $('select option[value="'+value+'"]').attr('data-vnama');

  $('#user-vnama').val(val);
  $('#user-vnip').val(value);
}

function verifikasi(obj){
  let repas = $(obj).val();
  let pass = $('#user-password').val();

  if(repas != pass){
    customAlert.gagal('re password tidak sama, periksa kembali form anda');
    $(obj).val('');
  }
}
</script>
