<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KunjunganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kunjungan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iId') ?>

    <?= $form->field($model, 'iMarketingId') ?>

    <?= $form->field($model, 'eType') ?>

    <?= $form->field($model, 'dChecked') ?>

    <?= $form->field($model, 'tKeterangan') ?>

    <?php // echo $form->field($model, 'vLongitude') ?>

    <?php // echo $form->field($model, 'vLatitude') ?>

    <?php // echo $form->field($model, 'tCreated') ?>

    <?php // echo $form->field($model, 'tUpdated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
