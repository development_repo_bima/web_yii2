<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Update */

$this->title = 'Tambah Keterangan Update Kehadiran';
$this->params['breadcrumbs'][] = ['label' => 'Update Kehadiran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="update-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
