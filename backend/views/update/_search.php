<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UpdateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="update-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iId') ?>

    <?= $form->field($model, 'vKeterangan') ?>

    <?= $form->field($model, 'iConseq') ?>

    <?= $form->field($model, 'tCreated') ?>

    <?= $form->field($model, 'tUpdated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
