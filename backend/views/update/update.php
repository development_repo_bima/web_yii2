<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Update */

$this->title = 'Update Keterangan Kehadiran: ' . $model->vKeterangan;
$this->params['breadcrumbs'][] = ['label' => 'Update Kehadiran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iId, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="update-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
