<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Outlet */

$this->title = 'Tambah Data Outlet';
$this->params['breadcrumbs'][] = ['label' => 'Outlet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outlet-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
