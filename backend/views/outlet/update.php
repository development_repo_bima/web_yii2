<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Outlet */

$this->title = 'Update Outlet: ' . $model->iId;
$this->params['breadcrumbs'][] = ['label' => 'List Outlet', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vNama, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="outlet-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
