<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OutletSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="outlet-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iId') ?>

    <?= $form->field($model, 'vCode') ?>

    <?= $form->field($model, 'vNama') ?>

    <?= $form->field($model, 'eGroup') ?>

    <?= $form->field($model, 'cTipe') ?>

    <?php // echo $form->field($model, 'eArea') ?>

    <?php // echo $form->field($model, 'iFieldforceId') ?>

    <?php // echo $form->field($model, 'iSpvId') ?>

    <?php // echo $form->field($model, 'iTOP') ?>

    <?php // echo $form->field($model, 'tAddress') ?>

    <?php // echo $form->field($model, 'vTelp') ?>

    <?php // echo $form->field($model, 'tCreated') ?>

    <?php // echo $form->field($model, 'tUpdated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
