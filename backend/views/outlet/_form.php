<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\select2\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\Outlet */
/* @var $form yii\widgets\ActiveForm */
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$isUpdate = (isset($_GET['id']) && $_GET['id']) ? true : false;

?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<script>var is_update = "<?= $isUpdate ?>"</script>
<div class="outlet-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'vCode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'vNama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'cTipe')->textInput(['maxlength' => true]) ?>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'eGroup')->dropDownList([ 'Swasta' => 'Swasta', 'Rekanan' => 'Rekanan', 'Pemerintah' => 'Pemerintah', 'Customers' => 'Customers', 'Principal' => 'Principal', ], ['prompt' => '']) ?>
          </div>
          <div class="col-md-6">
              <?= $form->field($model, 'eArea')->dropDownList([ 'JATENG' => 'JATENG', 'JABAR' => 'JABAR', 'JKT1' => 'JKT1', 'HO' => 'HO', 'JKT2' => 'JKT2', 'PKU' => 'PKU', 'MEDAN' => 'MEDAN', 'JATIM' => 'JATIM', 'BTM' => 'BTM', 'MTM' => 'MTM', 'JBM' => 'JBM', 'MKS' => 'MKS', 'ACH' => 'ACH', 'LPG' => 'LPG', 'PLB' => 'PLB', 'DPS' => 'DPS', 0 => '0', ], ['prompt' => '']) ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <?= $form->field($model, 'iCombo')->checkbox(['onchange'=>'comboDetect(this);']); ?>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-6" id="div-medrep-1">
            <?php
              echo $form->field($model, 'iFieldforceId')->widget(Widget::className(), [
                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Pilih NIP',
                    'onchange'   => 'clickable(this);',
                ],
                'settings' => [
                    'width' => '100%',
                ],
                'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                            ->where(['eDeleted'=>'Tidak','iActive'=>'1', 'iRole'=>'6'])
                            ->orderBy('vNama')->all(), 'iId', function($m){
                                return $m->vNama;
                            }),
                'events' => []
              ]);
            ?>
          </div>
          <div class="col-md-6" id="div-medrep-2">
            <?php
              echo $form->field($model, 'iFieldforceAddId')->widget(Widget::className(), [
                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Pilih NIP',
                    'onchange'   => 'clickable(this);',
                    
                ],

                'settings' => [
                    'width' => '100%',
                ],
                'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                            ->where(['eDeleted'=>'Tidak','iActive'=>'1', 'iRole'=>'6'])
                            ->orderBy('vNama')->all(), 'iId', function($m){
                                return $m->vNama;
                            }),
                'events' => []
              ]);
            ?>
          </div>
        </div>

      </div>
      <div class="col-md-6">
        <?php
          echo $form->field($model, 'iSpvId')->widget(Widget::className(), [
            'options' => [
                'multiple' => false,
                'placeholder' => 'Pilih NIP',
                'onchange'   => 'clickable(this);',
            ],
            'settings' => [
                'width' => '100%',
            ],
            'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                        ->where(['eDeleted'=>'Tidak','iActive'=>'1', 'iRole'=>'5'])
                        ->orderBy('vNama')->all(), 'iId', function($m){
                            return $m->vNama;
                        }),
            'events' => []
          ]);
        ?>

        <?= $form->field($model, 'iTOP')->textInput() ?>

        <?= $form->field($model, 'vTelp')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tAddress')->textarea(['rows' => 6]) ?>
      </div>
    </div>

    <!-- <?= $form->field($model, 'tCreated')->textInput() ?>

    <?= $form->field($model, 'tUpdated')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);
  // when hit update needed
  if($('#outlet-icombo').is(':checked')){
    $('#div-medrep-2').hide();
    $('#div-medrep-1').attr('class','col-md-12');
  }else{
    $('#div-medrep-2').show();
    $('#div-medrep-1').attr('class','col-md-6');
  }
});

function comboDetect(obj){
  if($(obj).is(':checked')){

    $('#div-medrep-2').hide();
    $('#div-medrep-1').attr('class','col-md-12');
  }else{

    $('#div-medrep-2').show();
    $('#div-medrep-1').attr('class','col-md-6');
  }
}
</script>
