<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KaryawanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="karyawan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iId') ?>

    <?= $form->field($model, 'vNip') ?>

    <?= $form->field($model, 'vNama') ?>

    <?= $form->field($model, 'vBirthPlace') ?>

    <?= $form->field($model, 'dBirth') ?>

    <?php // echo $form->field($model, 'tAddress') ?>

    <?php // echo $form->field($model, 'vTelp') ?>

    <?php // echo $form->field($model, 'iRole') ?>

    <?php // echo $form->field($model, 'iActive') ?>

    <?php // echo $form->field($model, 'dEntry') ?>

    <?php // echo $form->field($model, 'iContract') ?>

    <?php // echo $form->field($model, 'eDeleted') ?>

    <?php // echo $form->field($model, 'tCreated') ?>

    <?php // echo $form->field($model, 'tUpdated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
