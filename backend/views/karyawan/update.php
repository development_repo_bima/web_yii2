<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Karyawan */

$this->title = 'Update Karyawan: ' . $model->vNama;
$this->params['breadcrumbs'][] = ['label' => 'List Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vNama, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="karyawan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
