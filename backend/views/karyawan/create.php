<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Karyawan */

$this->title = 'Tambah Data Karyawan';
$this->params['breadcrumbs'][] = ['label' => 'List Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="karyawan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
