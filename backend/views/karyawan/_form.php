<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use buibr\datepicker\DatePicker;
use vova07\select2\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\Karyawan */
/* @var $form yii\widgets\ActiveForm */

$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$isUpdate = (isset($_GET['id']) && $_GET['id']) ? true : false;

?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<script>var is_update = "<?= $isUpdate ?>"</script>
<div class="karyawan-form" style="padding: 8px;">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'vNip')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'vNama')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'vBirthPlace')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'dBirth')->widget(
                DatePicker::className(), [
                    'addon' => false,
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'stepping' => 30,
                    ],
            ]);?>
          </div>
        </div>

        <?= $form->field($model, 'tAddress')->textarea(['rows' => 6]) ?>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'vTelp')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?php
            echo $form->field($model, 'iRole')->widget(Widget::className(), [
                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Pilih Jabatan',
                    // 'onchange'   => 'clickable(this);',

                ],
                'settings' => [
                    'width' => '100%',
                ],
                'items' => \yii\helpers\ArrayHelper::map(\app\models\Role::find()
                            ->where(['eDeleted'=>'Tidak','eAktif'=>'Ya'])
                            ->orderBy('vRoleName')->all(), 'iId', 'vRoleName'),
                'events' => []
              ]);
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'iActive')->dropDownList([ '1' => 'Aktif', '2' => 'Tidak Aktif', ]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'dEntry')->widget(
                DatePicker::className(), [
                    'addon' => false,
                    'size' => 'sm',
                    'clientOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'stepping' => 30,
                    ],
            ]);?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'iContract')->textInput(['type'=>'number']) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'iWorkLocation')->dropDownList([ '1' => 'TMP', '2' => 'PHC', ])->label("Lokasi Kerja") ?>
          </div>
        </div>
      </div>
    </div>

    <!-- <?= $form->field($model, 'eDeleted')->dropDownList([ 'Ya' => 'Ya', 'Tidak' => 'Tidak', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'tCreated')->textInput() ?>

    <?= $form->field($model, 'tUpdated')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);

});
</script>
