<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Produk */

$this->title = 'Update Produk: ' . $model->vNama;
$this->params['breadcrumbs'][] = ['label' => 'List Produk', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vNama, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="produk-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
