<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Produk */

$this->title = 'Tambah Data Produk';
$this->params['breadcrumbs'][] = ['label' => 'List Produk', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produk-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
