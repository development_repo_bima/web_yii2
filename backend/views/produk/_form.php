<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Produk */
/* @var $form yii\widgets\ActiveForm */
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$isUpdate = (isset($_GET['id']) && $_GET['id']) ? true : false;

?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<script>var is_update = "<?= $isUpdate ?>"</script>
<div class="produk-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'vCode')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'vNama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'vBrand')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'dHna')->textInput(['maxlength' => true,'type'=>'number']) ?>

      </div>
      <div class="col-md-6">
        <?= $form->field($model, 'eDistributor')->dropDownList([ 'TMP' => 'TMP', 'PHC' => 'PHC', ]) ?>
        <?= $form->field($model, 'vPrincipal')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'vKemasan')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'tExplain')->textarea(['rows' => 6]) ?>
      </div>
    </div>

    <!-- <?= $form->field($model, 'tCreated')->textInput() ?>

    <?= $form->field($model, 'tUpdated')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);

});
</script>
