<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Produk';
$this->params['breadcrumbs'][] = $this->title;
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<div class="card">
  <div class="card-title">
    <div class="pull-right" style="padding-left: 20px;padding-top: 10px;">
      <?= Html::a('Buat Data', ['create'], ['class' => 'btn btn-warning']) ?>
      <?= Html::a('All Data', ['index'], ['class' => 'btn btn-info']) ?>
      <?= Html::a('PHC', ['index-phc'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('TMP', ['index-tmp'], ['class' => 'btn btn-success']) ?>
    </div>

  </div>
  <div class="card-body">
    <div class="produk-index table-responsive">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyCell'=>'-',
        // 'layout'=>"{pager}\n{summary}\n{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'iId',
            'vCode',
            'vNama',
            'vBrand',
            'vPrincipal',
            'vKemasan',
            'dHna',
            'eDistributor',
            //'tCreated',
            //'tUpdated',
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{update}',
              'buttons' => [
                  'update' => function ($url,$model) {
                    return '<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-list"></i></a>
                    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                        <li>'.Html::a('Ubah', ['update?id='.$model->iId], ['class' => 'dropdown-item']).'</li>
                        <li class="dropdown-divider"></li>
                        <li>'.Html::a('Hapus', ['hapus?id='.$model->iId], ['class' => 'dropdown-item']).'</li>
                    </ul>';
                  }
              ],
            ],
        ],
        'tableOptions' =>['class' => 'table table-striped responsive'],
         'pager' => [
                  'options'=>['class'=>'pagination pull-right'],
                  'maxButtonCount'=>5,
                  'activePageCssClass' => 'page-item active',
                  'disabledPageCssClass' => 'disabled',
                  'linkOptions' => ['class' => 'page-link'],
          ],

    ]); ?>

    <?php Pjax::end(); ?>

    </div>
  </div>
</div>

<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);
});
</script>
