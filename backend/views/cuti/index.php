<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CutiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuti Karyawan';
$this->params['breadcrumbs'][] = $this->title;
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
?>
<script>var callback = <?php echo json_encode($encode); ?></script>
    <div class="card">
      <div class="card-title">
        <div class="pull-right" style="padding-left: 20px;padding-top: 10px;">
          <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

      </div>
      <div class="card-body">
        <div class="karyawan-index table-responsive">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyCell'=>'-',
            // 'layout'=>"{pager}\n{summary}\n{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'iKaryawanId',
                    'value'     => 'karyawan.vNama'
                ],
                // 'dPeriodeCuti',
                [
                    'attribute' => 'dPeriodeCuti',
                    //'format' => ['raw', 'Y-m-d H:i:s'],
                    'format' =>  ['date', 'php:Y-m-d'],
                    'options' => ['width' => '100']
                ],
                'vSemester',

                'iJumlah',
                'ePublish',
                // 'tCreated',
                [
                    'attribute' => 'tCreated',
                    'label'     => 'Dibuat Tanggal',
                    //'format' => ['raw', 'Y-m-d H:i:s'],
                    'format' =>  ['date', 'php:Y-m-d h:i:s'],
                    'options' => ['width' => '100']
                ],
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{update}',
                  'buttons' => [
                      'update' => function ($url,$model) {
                        return '<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-list"></i></a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li>'.Html::a('Ubah', ['update?id='.$model->iId], ['class' => 'dropdown-item']).'</li>

                            <!-- <li class="dropdown-divider"></li> -->
                        </ul>';
                      }
                  ],
                ],
            ],
            'tableOptions' =>['class' => 'table table-striped responsive'],
             'pager' => [
                      'options'=>['class'=>'pagination pull-right'],
                      'maxButtonCount'=>5,
                      'activePageCssClass' => 'page-item active',
                      'disabledPageCssClass' => 'disabled',
                      'linkOptions' => ['class' => 'page-link'],
              ],

        ]); ?>

        <?php Pjax::end(); ?>

        </div>
      </div>
    </div>
    <script>
    document.addEventListener("DOMContentLoaded", function(event) {
      component.showFlash(callback);
    });
    </script>
