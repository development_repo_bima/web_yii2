<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use buibr\datepicker\DatePicker;
use vova07\select2\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\Cuti */
/* @var $form yii\widgets\ActiveForm */
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$isUpdate = (isset($_GET['id']) && $_GET['id']) ? true : false;

?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<script>var is_update = "<?= $isUpdate ?>"</script>
<div class="cuti-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-6">
        <?php
          echo $form->field($model, 'iKaryawanId')->widget(Widget::className(), [
            'options' => [
                'multiple' => false,
                'placeholder' => 'Pilih NIP',
                'onchange'   => 'clickable(this);',
                // 'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                //             ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                //             ->orderBy('vNama')->all(), 'vNip', function($m){
                //                 return ['data-vnama'=>$m->vNama];
                //             }),
            ],
            'settings' => [
                'width' => '100%',
            ],
            'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                        ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                        ->orderBy('vNama')->all(), 'iId', function($m){
                            return $m->vNama .'('.$m->vNip.')';
                        }),
            'events' => []
          ]);
        ?>

        <?= $form->field($model, 'dPeriodeCuti')->widget(
            DatePicker::className(), [
                'addon' => false,
                'size' => 'sm',
                'clientOptions' => [
                    'format' => 'YYYY-MM-DD',
                    'stepping' => 30,
                ],
        ]);?>
      </div>
      <div class="col-md-6">
          <?= $form->field($model, 'iJumlah')->textInput() ?>

          <?= $form->field($model, 'ePublish')->dropDownList(['Ya'=>'Ya','Tidak'=>'Tidak']) ?>
      </div>
    </div>




    <!-- <?= $form->field($model, 'tCreated')->textInput() ?>

    <?= $form->field($model, 'tUpdated')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);

});
</script>
