<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cuti */

$this->title = 'Update Cuti: ' . $model->iId;
$this->params['breadcrumbs'][] = ['label' => 'Cuti Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iId, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cuti-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
