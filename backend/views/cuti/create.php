<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cuti */

$this->title = 'Tambah Cuti';
$this->params['breadcrumbs'][] = ['label' => 'Cutis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuti-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
