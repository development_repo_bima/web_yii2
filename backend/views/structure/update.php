<?php

/* @var $this yii\web\View */
/* @var $model app\models\TreeStructure */

$this->title = 'Update Tree Structure: ' . $model->iId;
$this->params['breadcrumbs'][] = ['label' => 'Tree Structures', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iId, 'url' => ['view', 'id' => $model->iId]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?=$this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>
            </div>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>