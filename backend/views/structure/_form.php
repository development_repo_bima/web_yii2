<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use vova07\select2\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\TreeStructure */
/* @var $form yii\bootstrap4\ActiveForm */
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
$update = (isset($_GET['id']) && $_GET['id']) ? false : true;
?>
<script>var callback = <?php echo json_encode($encode); ?></script>
<div class="tree-structure-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?php
                echo $form->field($model, 'iKaryawanId')->widget(Widget::className(), [
                    'options' => [
                        'multiple' => $update,
                        'placeholder' => 'Pilih NIP',
                        'onchange'   => 'clickable(this);',
                        'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                    ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                    ->orderBy('vNama')->all(), 'vNip', function($m){
                                        return ['data-vnama'=>$m->vNama];
                                    }),
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                    'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                ->orderBy('vNama')->all(), 'iId', function($m){
                                    return '('.$m->vNip.') - '.$m->vNama;
                                }),
                    'events' => []
                  ]);
            ?>
        </div>
        <div class="col-md-6">
            <?php
                echo $form->field($model, 'iAtasanSuperId')->widget(Widget::className(), [
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Pilih NIP Atasan',
                        'onchange'   => 'clickable(this);',
                        'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                    ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                    ->orderBy('vNama')->all(), 'vNip', function($m){
                                        return ['data-vnama'=>$m->vNama];
                                    }),
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                    'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                ->orderBy('vNama')->all(), 'iId', function($m){
                                    return '('.$m->vNip.') - '.$m->vNama;
                                }),
                    'events' => []
                  ]);
            ?>
        </div>
        <div class="col-md-6"></div>
        <div class="col-md-3">
            <?php
                echo $form->field($model, 'iAtasanLangsungId')->widget(Widget::className(), [
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Pilih NIP Atasan',
                        'onchange'   => 'clickable(this);',
                        'options' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                    ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                    ->orderBy('vNama')->all(), 'vNip', function($m){
                                        return ['data-vnama'=>$m->vNama];
                                    }),
                    ],
                    'settings' => [
                        'width' => '100%',
                    ],
                    'items' => \yii\helpers\ArrayHelper::map(\app\models\Karyawan::find()
                                ->where(['eDeleted'=>'Tidak','iActive'=>'1'])
                                ->orderBy('vNama')->all(), 'iId', function($m){
                                    return '('.$m->vNip.') - '.$m->vNama;
                                }),
                    'events' => []
                  ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'eJenis')->dropDownList([ 'Kantor' => 'Kantor', 'Lapangan' => 'Lapangan', ], ['prompt' => '']) ?>
        </div>
    </div>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  component.showFlash(callback);

});
</script>
