<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TreeStructureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Struktur Organisasi';
$this->params['breadcrumbs'][] = $this->title;
$encode = (Yii::$app->getSession()->getFlash('return')) ? Yii::$app->getSession()->getFlash('return') : "";
?>
<script>var callback = <?php echo json_encode($encode); ?></script>
    <div class="card">
      <div class="card-title">
        <div class="pull-right" style="padding-left: 20px;padding-top: 10px;">
          <?= Html::a('Buat Data', ['create'], ['class' => 'btn btn-warning']) ?>
        </div>

      </div>
      <div class="card-body">
        <div class="karyawan-index table-responsive">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyCell'=>'-',
            // 'layout'=>"{pager}\n{summary}\n{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'iKaryawanId',
                    'value'     => 'karyawan.vNama'
                ],
                [
                    'attribute' => 'iAtasanSuperId',
                    'value'     => 'super.vNama'
                ],
                [
                    'attribute' => 'iAtasanLangsungId',
                    'value'     => 'atasan.vNama'
                ],
                'eJenis',
                'eAktif',
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{update}',
                  'buttons' => [
                      'update' => function ($url,$model) {
                        $aktif = ($model->eAktif == "Ya") ? 'Tidak' : 'Ya';
                        return '<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-list"></i></a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li>'.Html::a('Ubah', ['update?id='.$model->iId], ['class' => 'dropdown-item']).'</li>
                            <li>'.Html::a(($model->eAktif == "Ya") ? "Non Aktifkan" : "Aktifkan", ['active?id='.$model->iId.'&param='.$aktif], ['class' => 'dropdown-item']).'</li>
                            
                        </ul>';
                      }
                  ],
                ],
            ],
            'tableOptions' =>['class' => 'table table-striped responsive'],
             'pager' => [
                      'options'=>['class'=>'pagination pull-right'],
                      'maxButtonCount'=>5,
                      'activePageCssClass' => 'page-item active',
                      'disabledPageCssClass' => 'disabled',
                      'linkOptions' => ['class' => 'page-link'],
              ],

        ]); ?>

        <?php Pjax::end(); ?>

        </div>
      </div>
    </div>
    <script>
    document.addEventListener("DOMContentLoaded", function(event) {
      component.showFlash(callback);
    });
    </script>
