<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TreeStructureSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row mt-2">
    <div class="col-md-12">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iId') ?>

    <?= $form->field($model, 'iKaryawanId') ?>

    <?= $form->field($model, 'iAtasanSuperId') ?>

    <?= $form->field($model, 'iAtasanLangsungId') ?>

    <?= $form->field($model, 'eJenis') ?>

    <?php // echo $form->field($model, 'eAktif') ?>

    <?php // echo $form->field($model, 'tCreated') ?>

    <?php // echo $form->field($model, 'tUpdated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
    <!--.col-md-12-->
</div>
