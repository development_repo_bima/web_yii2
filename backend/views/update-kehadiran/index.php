<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TCutiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update Kehadiran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
              <div class="card-title">
                <div class="pull-right" style="padding-left: 20px;padding-top: 10px;">
                  <!-- <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?> -->
                  <?= Html::a('Generate Report', ['create'], ['class' => 'btn btn-warning']) ?>
                </div>

              </div>
              <div class="card-body">
                <div class="cuti-index table-responsive">
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'emptyCell'=>'-',
                    // 'layout'=>"{pager}\n{summary}\n{items}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'iId',
                        [
                            'attribute' => 'iKaryawanId',
                            'value'     => 'karyawan.vNama'
                        ],
                        'eJenisCuti',
                        'iJumlah',
                        'tKeterangan',
                        'dPosted',
                        [
                          'attribute' => 'vLongitude',
                          'format'    => 'raw',
                          'value'     => function ($m){
                              return '<a href="http://maps.google.com/maps?q='.$m->vLatitude.','.$m->vLongitude.'" target="_blink">Lihat Lokasi</a>';
                          }
                        ],
                        [
                          'class' => 'yii\grid\ActionColumn',
                          'template' => '{update}',
                          'buttons' => [
                              'update' => function ($url,$model) {
                                return '<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-list"></i></a>
                                <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                    <li>'.Html::a('Ubah', ['update?id='.$model->iId], ['class' => 'dropdown-item']).'</li>

                                    <!-- <li class="dropdown-divider"></li> -->
                                </ul>';
                              }
                          ],
                        ],
                    ],
                    'tableOptions' =>['class' => 'table table-striped responsive'],
                     'pager' => [
                              'options'=>['class'=>'pagination pull-right'],
                              'maxButtonCount'=>5,
                              'activePageCssClass' => 'page-item active',
                              'disabledPageCssClass' => 'disabled',
                              'linkOptions' => ['class' => 'page-link'],
                      ],

                ]); ?>

                <?php Pjax::end(); ?>

                </div>
              </div>
            </div>
            <script>
            document.addEventListener("DOMContentLoaded", function(event) {
              component.showFlash(callback);
            });
            </script>
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
