<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sektor-form">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Ganti Password</h3>
		</div>
		<div class="panel-body">
			<div class="mprovinsi-form">

<?php $form = ActiveForm::begin(['id' => 'form-changepass']); ?>
            <?php echo Alert::widget(); ?>
            
             <?= $form->errorSummary($model,['header' => 'Please fix the following errors','class' => 'alert alert-danger']); ?>
            
            
       <div class="row">
           
           <div class="col-md-6 col-sm-6 col-xs-12">
   
        
        <fieldset>
       	<?= $form->field($model, 'oldpassword')->passwordInput(['maxlength' => true]); ?>
       	<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]); ?>
       	<?= $form->field($model, 'repeatnewpass')->passwordInput(['maxlength' => true]); ?>
       
               
       
 </div>
					<div class="col-md-6 col-md-offset-6">
						<div class="pull-right">
							<div class="form-group">
								<?=  Html::a('<i class="fa fa-arrow-circle-left "></i> Kembali',['index'],['class' => 'btn btn-success'])
								?>
								<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
							</div>
						</div>
					</div>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>