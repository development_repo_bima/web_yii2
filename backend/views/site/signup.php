<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\web\JsExpression;
use app\models\Cabang;

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' + 
   
    '<div class="col-sm-5">' + repo.text + '</div>' +
    '<div class="col-sm-5">' + repo.description + '</div>' +
'</div>';   
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.id || repo.name;
}
JS;
$this->registerJs($formatJs, View::POS_HEAD);

$resultsJs = <<< JS
 function (data, params) {
        params.page = params.page || 1;
        return {
            // Change `data.items` to `data.results`.
            // `results` is the key that you have been selected on
            // `actionJsonlist`.
            results: data.results
        };
    }
JS;

?>
<div class="bank-form">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Create User</h3>
	</div>
	<div class="panel-body">
		<div class="customer-form">


            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                     <div class="row">
                    <div class="col-md-6">
                        <?= $form->errorSummary($model); ?>
             <?= $form->errorSummary($model); ?>        
              
            
            <?php
            $url2 = \yii\helpers\Url::to(['employee']);
            echo $form->field($model, 'username')->widget(Select2::classname(), [
                //'value' => '14719648',
                //'initValueText' => 'kartik-v/yii2-widgets',
                'options' => ['placeholder' => 'Search Produk'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => $url2,
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                        'processResults' => new JsExpression($resultsJs),
                        'cache' => true
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('formatRepo'),
                    'templateSelection' => new JsExpression('formatRepoSelection'),
                ],
            ]);
            ?>
          
 

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'repassword')->passwordInput() ?>
              
                      <?= $form->field($model, 'cabang')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Cabang::find()->all(),'iId','vNama'),
                                'options' => ['placeholder' => 'Pilih Cabang'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                    
                     <?= $form->field($model, 'allcabang')->dropDownList([ 'Ya' => 'Ya', 'tidak' => 'Tidak', ], ['prompt' => '']) ?>
              

                    </div>
                    <div class="col-md-6 col-md-offset-6">
                        <div class="pull-right">
                            <div class="form-group">
                                <?=  Html::a('<i class="fa fa-arrow-circle-left "></i> Kembali',['index'],['class' => 'btn btn-success']) ?>
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>