<?php

use dosamigos\chartjs\ChartJs;

$this->title = 'Dashboard SAP';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-lg-6">
            <?= \hail812\adminlte3\widgets\Alert::widget([
                'type' => 'success',
                'body' => '<h3>Congratulations!</h3>',
            ]) ?>
            <?= \hail812\adminlte3\widgets\Callout::widget([
                'type' => 'danger',
                'head' => 'I am a danger callout!',
                'body' => 'There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.'
            ]) ?>
        </div> -->

        <div class="col-md-6">
          <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'height' => 30,
                'width' => 30
            ],
            'data' => [
                'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                'datasets' => [
                    [
                        'label' => "My First dataset",
                        'backgroundColor' => "rgba(179,181,198,0.2)",
                        'borderColor' => "rgba(179,181,198,1)",
                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                        'data' => [65, 59, 90, 81, 56, 55, 40]
                    ],
                    [
                        'label' => "My Second dataset",
                        'backgroundColor' => "rgba(255,99,132,0.2)",
                        'borderColor' => "rgba(255,99,132,1)",
                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                        'data' => [28, 48, 40, 19, 96, 27, 100]
                    ]
                ]
            ]
        ]);
        ?>
        </div>
        <div class="col-md-6" width="50%">
          <?php
          echo ChartJs::widget([
              'type' => 'pie',
              'id' => 'structurePie',
              'options' => [
                  'height' => 10,
                  'width' => 20,
              ],
              'data' => [
                  'radius' =>  "90%",
                  'labels' => ['Label 1', 'Label 2', 'Label 3'], // Your labels
                  'datasets' => [
                      [
                          'data' => ['35.6', '17.5', '46.9'], // Your dataset
                          'label' => '',
                          'backgroundColor' => [
                                  '#ADC3FF',
                                  '#FF9A9A',
                              'rgba(190, 124, 145, 0.8)'
                          ],
                          'borderColor' =>  [
                                  '#fff',
                                  '#fff',
                                  '#fff'
                          ],
                          'borderWidth' => 1,
                          'hoverBorderColor'=>["#999","#999","#999"],
                      ]
                  ]
              ],
              'clientOptions' => [
                  'legend' => [
                      'display' => false,
                      'position' => 'bottom',
                      'labels' => [
                          'fontSize' => 14,
                          'fontColor' => "#425062",
                      ]
                  ],
                  'tooltips' => [
                      'enabled' => true,
                      'intersect' => true
                  ],
                  'hover' => [
                      'mode' => false
                  ],
                  'maintainAspectRatio' => false,

              ],
            ]);
          ?>
        </div>
    </div>
</div>
