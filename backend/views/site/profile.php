<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'User Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mprovinsi-view">

    <div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Profile - view</h3>
        </div>
        
        <div class="panel-body">
                
   
            
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            
            [
                'attribute' => 'name',
                'value' => $model->employee->vName,
            ],
            [
                'attribute' => 'NIP',
                 'value' => $model->employee->cNip,
            ],
            [
                'attribute' => 'Worklocation',
                'value' => $model->employee->worklocation->V_LOCATION_NAME,
            ],
            [
                'attribute' => 'Company',
                'value' => $model->employee->company->vCompName,
            ],
            [
                'attribute' => 'cabang',
                'value' => $model->cabang->vNama,
            ],
             
            [
                'attribute' => 'Role',
                'value' => $model->role->item_name,
            ],
            'eAllcabang',
           
   ],
  ]) ?>
  <?=  Html::a('<i class="fa fa-arrow-circle-left "></i> Ganti Password',['gantipass'],['class' => 'btn btn-success']) ?>          
  <?=  Html::a('<i class="fa fa-arrow-circle-left "></i> Kembali',['index'],['class' => 'btn btn-success']) ?>
   

</div>
</div>
</div>