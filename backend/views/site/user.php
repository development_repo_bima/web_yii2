<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use mdm\admin\components\Helper;
use common\widgets\Alert;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SektorSearch $searchModel
 */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>

<div class="sektor-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a('Create Sektor', ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>
    <?php     $create = ''; ?>
    <?php     if(Helper::checkRoute('/site/signup')){
          $create =  Html::a('<i class="glyphicon glyphicon-plus"></i>', ['signup'], ['class' => 'btn btn-success']);
    }?>
   
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          
            'username',
            //'email',
            [
                'attribute' => 'rolename',
                'value' => 'role.item_name'
            ],
            [
                'attribute' => 'worklocationid',
                'value' => 'employee.worklocation.V_LOCATION_NAME'
            ],
            
            'name',
             [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->status == 0 ? 'Inactive' : 'Active';
                },
                'filter' => [
                    0 => 'Inactive',
                    10 => 'Active'
                ]
            ],
//            'eDeleted', 
//            'iCreatedid', 
//            ['attribute' => 'tCreated','format' => ['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'iUpdatedid', 
//            ['attribute' => 'tUpdated','format' => ['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'foxid', 

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Helper::filterActionColumn('{view} {userupdate}'),
                'buttons' => [
                     'view' => function ($url, $model) {
                     $icon = '<span class="glyphicon glyphicon-eye-open btn btn-sm btn-primary"></span>';
                     return Html::a($icon,$url);             
                    },
                    
                    'userupdate' => function ($url, $model) {
                      
                    $icon = '<span class="glyphicon glyphicon-pencil btn btn-sm btn-warning"></span>';
                    return Html::a($icon,$url);
                    },
                    
                    'delete' => function ($url, $model) {
                    $icon = '<span class="glyphicon glyphicon-trash "></span>';
                    return Html::a('<span class="glyphicon glyphicon-trash btn btn-sm btn-danger"></span>', $url, [
                           'title' => 'Confirmation Delete',
                           'data-method' => 'post',
                           'data-pjax' => 1,
                           'data-confirm' => 'Are you sure you want to delete this item?',
                         ]);
                       },
                    
                ],
            ],
        ],
         'containerOptions' => ['style' => 'overflow: auto'],
         'headerRowOptions' => ['class' => 'kartik-sheet-style'],
         'filterRowOptions' => ['class' => 'kartik-sheet-style'],
         'pjax' => true,
         
         'toolbar' =>  [
             
            ['content' => 
            
                $create. ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/site/userindex'], ['class' => 'btn btn-warning', 'title' => Yii::t('kvgrid', 'Reset Grid')
                    ])
              
           ],
              
            '{export}',
            '{toggleData}',
            
        ],
        
        'export' => [
            'fontAwesome' => true
        ],
         
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
         'pager' => [
                    'options'=>['class'=>'pagination'],   
                    'prevPageLabel' => '<i class="glyphicon glyphicon-step-backward"></i>',  
                    'nextPageLabel' => '<i class="glyphicon glyphicon-step-forward"></i>',   
                    'firstPageLabel'=>'<i class="glyphicon glyphicon-fast-backward"></i>',  
                    'lastPageLabel'=>'<i class="glyphicon glyphicon-fast-forward"></i>',   
                    'maxButtonCount'=>10,    
            ],      

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'User List',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
        'exportConfig' => 'excel',
        
    ]); ?>

</div>