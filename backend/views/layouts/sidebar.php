<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=\yii\helpers\Url::home()?>" class="brand-link">
        <img src="<?=$assetDir?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">TPS</span><br>
        <small style="padding-left: 50px;"><i>Total Prima Sistem</i></small>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">

            <?php
            echo \hail812\adminlte3\widgets\Menu::widget([
                'items' => [
                    ['label' => 'Dashboard', 'url' => ['/'], 'iconStyle' => 'far'],
                    [
                        'label' => 'Master Data',
                        'icon' => 'database',
                        // 'badge' => '<span class="right badge badge-info">2</span>',
                        'items' => [
                            ['label' => 'Master User', 'url' => ['user/index'], 'iconStyle' => 'fas', 'icon'=>'key'],
                            [
                              'label' => 'Master Karyawan',
                              'iconStyle' => 'fa',
                              'icon'=>'users',
                              'items' => [
                                ['label'=>'All Data', 'url' => ['karyawan/index'], 'iconStyle'=>'fas','icon'=>'user'],
                                ['label'=>'TMP', 'url' => ['karyawan/index-tmp'], 'iconStyle'=>'fas','icon'=>'user'],
                                ['label'=>'PHC', 'url' => ['karyawan/index-phc'], 'iconStyle'=>'fas','icon'=>'user'],
                              ]
                            ],
                            ['label' => 'Master Cuti Karyawan', 'url' => ['cuti/index'], 'iconStyle' => 'fa', 'icon'=>'car'],
                            [
                              'label' => 'Master Outlet',
                              'iconStyle' => 'fa',
                              'icon'=>'suitcase',
                              'items' => [
                                ['label'=>'All Data', 'url' => ['outlet/index'], 'iconStyle'=>'fas','icon'=>'check'],
                                ['label'=>'TMP', 'url' => ['outlet/index-tmp'], 'iconStyle'=>'fas','icon'=>'check'],
                                ['label'=>'PHC', 'url' => ['outlet/index-phc'], 'iconStyle'=>'fas','icon'=>'check'],
                                ['label'=>'Combo', 'url' => ['outlet/index-combo'], 'iconStyle'=>'fas','icon'=>'check'],
                              ]
                            ],
                            [
                              'label' => 'Master Produk',
                              'iconStyle' => 'fa',
                              'icon'=>'life-ring',
                              'items' => [
                                ['label'=>'All Data', 'url' => ['produk/index'], 'iconStyle'=>'fas','icon'=>'check'],
                                ['label'=>'TMP', 'url' => ['produk/index-tmp'], 'iconStyle'=>'fas','icon'=>'check'],
                                ['label'=>'PHC', 'url' => ['produk/index-phc'], 'iconStyle'=>'fas','icon'=>'check'],
                              ]
                            ],
                            ['label' => 'Master Jabatan', 'url' => ['role/index'], 'iconStyle' => 'fa', 'icon'=>'sitemap'],
                            ['label' => 'Struktur Organisasi', 'url' => ['structure/index'], 'iconStyle' => 'fa', 'icon'=>'sitemap'],
                            ['label' => 'Master Update Kehadiran', 'url' => ['update/index'], 'iconStyle' => 'fa', 'icon'=>'rss'],
                        ]
                    ],
                    [
                        'label' => 'Absensi',
                        'icon' => 'map-pin',
                        // 'badge' => '<span class="right badge badge-info">2</span>',
                        'items' => [
                            ['label' => 'Absensi Karyawan', 'url' => ['absensi/index'], 'iconStyle' => 'fa', 'icon'=>'user'],
                            ['label' => 'Absensi Marketing', 'url' => ['kunjungan/index'], 'iconStyle' => 'fa', 'icon'=>'suitcase'],
                            ['label' => 'Absensi Kehadiran', 'url' => ['update-kehadiran/index'], 'iconStyle' => 'fa', 'icon'=>'paper-plane'],
                        ]
                    ],
                    // ['label' => 'Simple Link', 'icon' => 'th', 'badge' => '<span class="right badge badge-danger">New</span>'],
                    // ['label' => 'Yii2 PROVIDED', 'header' => true],
                    // ['label' => 'Login', 'url' => ['site/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    // ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank'],
                    // ['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug'], 'target' => '_blank'],
                    // ['label' => 'MULTI LEVEL EXAMPLE', 'header' => true],
                    // ['label' => 'Level1'],
                    // [
                    //     'label' => 'Level1',
                    //     'items' => [
                    //         ['label' => 'Level2', 'iconStyle' => 'far'],
                    //         [
                    //             'label' => 'Level2',
                    //             'iconStyle' => 'far',
                    //             'items' => [
                    //                 ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                    //                 ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                    //                 ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle']
                    //             ]
                    //         ],
                    //         ['label' => 'Level2', 'iconStyle' => 'far']
                    //     ]
                    // ],
                    // ['label' => 'Level1'],
                    // ['label' => 'LABELS', 'header' => true],
                    // ['label' => 'Important', 'iconStyle' => 'far', 'iconClassAdded' => 'text-danger'],
                    // ['label' => 'Warning', 'iconClass' => 'nav-icon far fa-circle text-warning'],
                    // ['label' => 'Informational', 'iconStyle' => 'far', 'iconClassAdded' => 'text-info'],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
