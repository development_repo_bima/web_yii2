customAlert = {
    sukses: function (msg) {
      return Swal.fire(
        'Yeeayy',
        msg,
        'success'
      );
    },
    gagal: function (msg) {
      return Swal.fire(
        'Ooopps',
        msg,
        'error'
      );
    }
};

component = {
  showFlash : function (callback){
    if(callback){

      if(callback.status){
        customAlert.sukses(callback.msg);
      }else{
        customAlert.gagal(callback.msg);;
      }
    }
  }
}
