<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.16.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-bootstrap4' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap4',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/bootstrap4' => $vendorDir . '/yiisoft/yii2-bootstrap4/src',
    ),
  ),
  'hail812/yii2-adminlte3' => 
  array (
    'name' => 'hail812/yii2-adminlte3',
    'version' => '1.0.8.0',
    'alias' => 
    array (
      '@hail812/adminlte3' => $vendorDir . '/hail812/yii2-adminlte3/src',
    ),
  ),
  '2amigos/yii2-chartjs-widget' => 
  array (
    'name' => '2amigos/yii2-chartjs-widget',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@dosamigos/chartjs' => $vendorDir . '/2amigos/yii2-chartjs-widget/src',
    ),
  ),
  'buibr/yii2-datepicker-bs4' => 
  array (
    'name' => 'buibr/yii2-datepicker-bs4',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@buibr/datepicker' => $vendorDir . '/buibr/yii2-datepicker-bs4',
    ),
  ),
  'vova07/yii2-select2-widget' => 
  array (
    'name' => 'vova07/yii2-select2-widget',
    'version' => '0.1.2.0',
    'alias' => 
    array (
      '@vova07/select2' => $vendorDir . '/vova07/yii2-select2-widget/src',
    ),
  ),
  'codemix/yii2-excelexport' => 
  array (
    'name' => 'codemix/yii2-excelexport',
    'version' => '2.7.2.0',
    'alias' => 
    array (
      '@codemix/excelexport' => $vendorDir . '/codemix/yii2-excelexport/src',
    ),
  ),
);
